<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/getGallery/{page?}', 'HomeController@getGallery');
Route::post('/', 'HomeController@submit')->name('submit');
Route::get('sendEmail/{submission_id?}', 'HomeController@sendEmail');
Route::post('/facebookLogin', 'HomeController@facebookLogin')->name('facebookLogin');
Route::post('/facebookShare', 'HomeController@facebookShare')->name('facebookShare');

Route::post('/checkUserHasShared', 'HomeController@checkUserHasShared')->name('checkUserHasShared');

// Route::get('/init', function () {
//     return Artisan::call('migrate');
// });
Route::get('/thank-you-email/', 'HomeController@testEmail')->name('thank-you');
Route::get('/thank-you/', 'HomeController@thankYou')->name('thank-you');
Route::get('/thank-you/{code}', 'HomeController@thankYou')->name('thank-you');
Route::get('/redirect/{target}', 'HomeController@redirect')->name('redirect');

Route::prefix('admin')->group(function () {
    Route::get('/', function () {
        return redirect()->route('admin-dashboard');
    });

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::post('/save-date', 'AdminController@saveDate')->name('save-date');

    Route::get('/dashboard', 'AdminController@index')->name('admin-dashboard');
    Route::get('/resend-email/{code}', 'AdminController@resendEmail')->name('resend-email');
    Route::get('/generate-code/{amount}', 'AdminController@generateUniqueCodes')->name('generate-code');
    Route::get('/export-registration-list', 'AdminController@exportRegistrationList')->name('export-registration-list');
    Route::get('/export-code-list', 'AdminController@exportCodeList')->name('export-code-list');

    Route::prefix('datatables')->group(function () {
        Route::get('/total-registration', 'AdminController@datatablesTotalRegistration')->name('datatables-total-registration');
        Route::get('/registration-list', 'AdminController@datatablesRegistrationList')->name('datatables-registration-list');
    });
});
