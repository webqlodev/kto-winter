<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Submission extends BaseModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_submission';
}
