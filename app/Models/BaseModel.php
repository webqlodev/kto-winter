<?php

namespace App\Models;

class BaseModel extends \Illuminate\Database\Eloquent\Model
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];
}
