<?php

namespace App\Mail;

use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ThankYouForRegistering extends Mailable
{
    use Queueable, SerializesModels;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code,$name,$dateofbirth,$email,$mobileno,$type,$img,$imgDesc)
    {
        $this->code = $code;
        $this->name = $name;
        $this->dateofbirth = $dateofbirth;
        $this->email = $email;
        $this->mobileno = $mobileno;
        $this->type = $type;
        $this->img = $img;
        $this->imgDesc = $imgDesc;
        //DB::table('registrations')->where('unique_code', $code)->first();
        //$this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from([env('EMAIL_FROM_NAME') => env('EMAIL_FROM')])
            ->subject(env('EMAIL_SUBJECT'))
            ->view('email.voucher')
            ->with([
                'code' => $this->code,
                'name' => $this->name,
                'dateofbirth' => $this->dateofbirth,
                'mobileno' => $this->mobileno,
                'email' => $this->email,
                'sense' => $this->type,
                'photo' => $this->img,
                'caption' => $this->imgDesc
            ]);
    }
}
