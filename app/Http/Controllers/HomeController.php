<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Validator;
use App\Mail\ThankYouForRegistering;
use App\Models\Form;
use App\Models\Submission;
use App\Models\Social;
use Illuminate\Http\Request;
use App\Libraries\ApiResponse;
use Carbon\Carbon;
//use Spatie\Browsershot\Browsershot; //required php7.1


class HomeController extends Controller
{
    /**
     * Show the application home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $end = DB::table('campaign')->where('key', 'end_time')->value('value');
        // $start = DB::table('campaign')->where('key', 'start_time')->value('value');
        // if ( Carbon::parse( env('END_TIME') )->isFuture() ) {
        if ( Carbon::parse($end)->isFuture() ) {
            return view('home');
        } else {
            return view('end', ['end'=>$end] );
        }
    }

    public function generateError($b_success,$t_field_name,$t_message){
        $arr_msg['bSuccess'] = $b_success;
        $arr_msg[$t_field_name] = $t_message;
        return $arr_msg;
    }

    public function storeSubmission($t_form_id,$t_social_id){
        $submission = new Submission();
        $submission->form_id = $t_form_id;
        $submission->t_social_id = $t_social_id;
        try{
            $submission->save();
            $arr_results = $this->generateError(1,'submissionId',$submission->id);
            return $arr_results;  
        }catch(\Exception $e){

            $arr_results = $this->generateError(0,'tMessage','Fail to store submission');
            return $arr_results;
        }
    }

    public function storeForm(Request $request)
    {
        $form = new Form();

        $d_date_of_birth = $request->get('dateOfBirth');
        $date = str_replace('/', '-', $d_date_of_birth);
        $d_converted_date_of_birth = date('Y-m-d', strtotime($date));

        $form->t_full_name = $request->get('fullName');
        $form->t_email = $request->get('email');
        $form->t_mobile_no = $request->get('mobileNo');
        $form->t_type = $request->get('type');
        $form->t_img_desc = $request->get('imgDesc');
        $form->b_subscribe = $request->get('subscribe');
        $form->d_date_of_birth = $d_converted_date_of_birth;

        try{
            $form->save();
            $arr_results = $this->generateError(1,'formId',$form->id);
            return $arr_results;  
        }catch(\Exception $e){
            $arr_results = $this->generateError(0,'tMessage','Fail to submit form');
            return $arr_results;
        }
        

        
    }

    public function storeFacebook(Request $request){
        $social = new Social();

        $social->t_social_id = $request->get('socialId');
        $social->t_full_name = $request->get('fullName');
        $social->t_email = $request->get('email');

        try{
            $social->save();
            $arr_results = $this->generateError(1,'tMessage','Success');
            return $arr_results; 
        }catch(\Exception $e){
            $arr_results = $this->generateError(0,'tMessage','Something is wrong with the server. Please try again later');
            return $arr_results;
        }
        
    }

    public function getGallery(Request $request, $page = 1){
        $api = new ApiResponse();
        $take = 6;

        $skip_total = ($page - 1) * $take;

        $galleryDesc = Form ::orderBy('created_at', 'DESC')
                        ->offset($skip_total)
                        ->take($take)
                        ->get();
        $galleryTotalRow = Form::count('id');
        $galleryTotal = round($galleryTotalRow /6);

        $tUrl = $request->root();
        if(!$galleryDesc->isEmpty()){
            foreach($galleryDesc as $index=>$arr_data){
                $arr_org_data = $arr_data['original'];
                $arr_org_data['original']['img'] = $tUrl.'/storage/files/'.$arr_data['original']['t_img'];
                $arr_org_data['original']['img_desc'] = $arr_data['original']['t_img_desc'];
                $arr_org_data['original']['id'] = $arr_data['original']['id'];
                $arr_org_data['original']['total_page'] = $galleryTotal;
                $arr_result[] = $arr_org_data['original'];
                $arr_condition[$arr_data['original']['id']] = $arr_data['original']['id'];
            }

            
      

        }else{
            $arr_result = array();
        }
                             
        $api->setResult($arr_result);

       return $api->get();

    }

    // public function getGallery(Request $request){
    //     $api = new ApiResponse();
    //     $galleryDesc = Form::orderBy('created_at', 'DESC')
    //             ->take(3)->get();
    //     $tUrl = $request->root();
    //     if(!$galleryDesc->isEmpty()){
    //         foreach($galleryDesc as $index=>$arr_data){

    //            //$arr_data['original']['t_img'] = storage_path('files/'.$arr_data['original']['t_img']);
    //             $arr_org_data = $arr_data['original'];
    //             $arr_org_data['original']['t_img'] = $tUrl.'/storage/files/'.$arr_data['original']['t_img'];
    //             $arr_org_data['original']['t_img_desc'] = $arr_data['original']['t_img_desc'];
    //             $arr_result[] = $arr_org_data['original'];
    //             $arr_condition[$arr_data['original']['id']] = $arr_data['original']['id'];
    //         }

    //        $galleryRand = Form::whereNotNull('t_img')
    //                         ->inRandomOrder()
    //                         ->take(6)
    //                         ->whereNotIn('id', $arr_condition)
    //                         ->get();
    //         foreach($galleryRand as $index=>$arr_rand_data){
    //             $arr_org_data = $arr_rand_data['original'];
    //             $arr_org_data['original']['t_img'] = $tUrl.'/storage/files/'.$arr_rand_data['original']['t_img'];
    //             $arr_org_data['original']['t_img_desc'] = $arr_rand_data['original']['t_img_desc'];
    //             $arr_result[] = $arr_org_data['original'];
    //         }      
    //     }else{
    //         $arr_result = array();
    //     }
                             
    //     $api->setResult($arr_result);

    //    return $api->get();

    // }

    public function getUserFacebookRecord($t_social_id){
        $social = Social::where('t_social_id', $t_social_id)
                ->first();
        if(!empty($social)){
            $arr_results = $this->generateError(1,'tMessage','User exist');
        }else{
            $arr_results = $this->generateError(0,'tMessage','User does not exist');
        }
        return $arr_results; 
    }

    public function getUserShareRecord($t_social_id){
        $social = Social::where('t_social_id', $t_social_id)
                    ->where('b_share',1)
                    ->first();
        if(!empty($social)){
            $arr_results = $this->generateError(1,'tMessage','User shared');
        }else{
            $arr_results = $this->generateError(0,'tMessage','User does not exist');
        }
        return $arr_results; 
    }
    
    public function updateForm($t_form_id,$t_img){

        $form = Form::where('id',$t_form_id)->first();

        if(!empty($form)){     

            $form->t_img = $t_img;

            $form->save();

            $arr_results = $this->generateError(1,'tMessage','Success');
        }else{
            $arr_results = $this->generateError(0,'tMessage','Something is wrong with the server. Please try again later');
        }

        return $arr_results;

    }

    public function updateSubmissionSocialId($t_social_id,$t_submission_id){

        $submission = Submission::find($t_submission_id);
        if(!empty($submission)){     

            $submission->t_social_id = $t_social_id;

            $submission->save();

            $arr_results = $this->generateError(1,'tMessage','Success');
        }else{
            $arr_results = $this->generateError(0,'tMessage','Something is wrong with the server. Please try again later');
        }

        return $arr_results;
    }



    public function updateSocialShare($t_social_id){
        //$submission = new Submission();
        $social = Social::where('t_social_id',$t_social_id)->first();
//dd($t_social_id,$social);
        if(!empty($social)){
            $social->b_share = 1;
            $social->save();
            $arr_results = $this->generateError(1,'tMessage','Success');
       }else{
            $arr_results = $this->generateError(0,'tMessage','Something is wrong with the server. Please try again later');
       }
       return $arr_results;
    }

    public function facebookShare(Request $request){
        $api = new ApiResponse();
        $t_social_id = $request->get('socialId');
        $arr_update_result = $this->updateSocialShare($t_social_id);
        if($arr_update_result['bSuccess'] == 0){
            $api->setStatus(400);
            $api->setError($arr_update_result);
            return $api->get();
        }
        $arr_update_result['promoCode'] = 'XYZCB';
        $api->setResult($arr_update_result);
        return $api->get();
    }

    public function checkUserHasShared(Request $request){
        $api = new ApiResponse();
        $social = Social::where('t_social_id',$request->get('socialId'))->first();

        if(!empty($social)){
            $arr_results = $this->generateError(1,'bUserIsShare',$social->b_share);
       }else{
            $arr_results = $this->generateError(0,'tMessage','Something is wrong with the server. Please try again later');
       }
       $arr_results['promoCode'] = 'XYZCB';
       $api->setResult($arr_results);

       return $api->get();
    }

    public function facebookLogin(Request $request){
        $api = new ApiResponse();

        $t_social_id = $request->get('socialId');
        $t_form_id = $request->get('submissionId');
        $arr_update_submisison = $this->updateSubmissionSocialId($t_social_id,$t_form_id);

        if($arr_update_submisison['bSuccess'] == 0){
            $api->setStatus(400);
            $api->setError($arr_update_submisison);
            return $api->get();
        }

        $arr_get_social = $this->getUserFacebookRecord($t_social_id);
        if($arr_get_social['bSuccess'] == 0){
            $arr_return_store = $this->storeFacebook($request);
            if($arr_return_store['bSuccess'] == 0){
                $api->setStatus(400);
                $api->setError($arr_return_store);
                return $api->get();
            }
            $arr_get_social['bUserIsShare'] = 0;
        }else{
            $arr_get_share = $this->getUserShareRecord($t_social_id);
            if($arr_get_share['bSuccess'] == 0){
                $arr_get_social['bUserIsShare'] = 0;
            }else{
                $arr_get_social['bUserIsShare'] = 1;
            }
            
        }
        $api->setResult($arr_get_social);

        return $api->get();

    }

    public function submit(Request $request)
    {
       
        $api = new ApiResponse();
        $validator = Validator::make($request->all(), [
            'fullName' => 'required',
            'dateOfBirth' => 'required',
            'mobileNo' => 'required',
            'termsCondition' => 'required',
            'type' => 'required',
            'img' => 'required',
    		'email' => 'required|email|max:255|unique:registrations,email',
        ], [
            'fullName.required'=> 'Name is required',
            'dateOfBirth.required'=> 'Date Of Birth is required',
            'mobileNo.required'=> 'Mobile No is required',
            'img.required' => 'Image is required',
            'type.required' => 'Sense of Korea is required',
            'termsCondition.required'=> 'Terms & Condition is required',
            'email.required' => 'Email address is required to participate.',
            'email.email' => 'Email address must be valid.',
            'email.max' => 'Email address is too long.',
        ]);

        if ($validator->fails()) {
            $api->setStatus(400);
            $api->setError($validator->errors());
            return $api->get();
        }

        $arr_return_results = $this->storeForm($request);
        if($arr_return_results['bSuccess'] == 0){
            $api->setStatus(400);
            $api->setError($arr_return_results);
            return $api->get();
        }
        $t_form_id = $arr_return_results['formId'];


        $arr_store_submission = $this->storeSubmission($t_form_id,$request->get('socialId'));

        $t_img_name = $t_form_id.'_'.date('YmdHis').'.jpg';
        $imageData = $request->get('img');
        $convertedImg = $this->convertDownloadBaseToImg($imageData, $t_img_name);

        $arr_update_form = $this->updateForm($t_form_id,$t_img_name);

        if($arr_update_form['bSuccess'] == 0){
            $api->setStatus(400);
            $api->setError($arr_update_form);
            return $api->get();
        }
        $arr_results['submissionId'] = $arr_store_submission['submissionId'];
        $api->setResult($arr_results);

        return $api->get();
    }



    public function convertDownloadBaseToImg($t_base64_string, $t_img_name){
       
        $data = explode( ',', $t_base64_string );
        $img_data = base64_decode( $data[ 1 ] );
        $html_file = public_path('storage/files/'.$t_img_name);

        if(file_put_contents($html_file, $img_data)){
            return true;
        }else{
            return false;
        }

    }

    public function thankYou($code)
    {
        $registration = DB::table('registrations')->where('unique_code', $code)->first();

        if ($registration) {
            return view('tq', [
                'code' => $code,
            ]);
        } else {
            abort(404);
        }
    }

    // public function thankYou()
    // {
    //     return view('tq');
    // }

    public function redirect($target)
    {
        DB::table('clicks')->insert([
            'target' => $target,
            'created_at' => Carbon::now(),
        ]);

        switch ($target) {
            case 'facebook':
            case 'facebook-email':
                $url = 'https://www.facebook.com/MyHeycha/';
                break;
        }

        return redirect($url);
    }

    public function sendEmail(Request $request,$submission_id){
        $getFormId = Submission :: where('id','=',$submission_id)
                                ->first();
        if(!empty($getFormId)){
            $tUrl = $request->root();
            $form_id = $getFormId->form_id;
            $get_email = Form :: where('id','=',$form_id)
                                ->first();
            $email = $get_email->t_email;
            $name = $get_email->t_full_name;
            $dateofbirth = $get_email->d_date_of_birth;
            $mobileno = $get_email->t_mobile_no;
            $type = $get_email->type;
            $img = $tUrl.'/storage/files/'.$get_email->t_img;
            $imgDesc = $get_email->t_img_desc;
            Mail::to( $email )->send( new ThankYouForRegistering('XYZCB',$name,$dateofbirth,$email,$mobileno,$type,$img,$imgDesc) );
        }
        
    }   


    public function testEmail() // email method, email screenshot method.
    {
        $email = ['novadean90@gmail.com'];
        $code = '00001';
        $user = DB::table('registrations')->where('unique_code', $code)->first();
        // Mail::to( $email )->send( new ThankYouForRegistering($code) );

        // Browsershot::html(view('email.thank-you',['user'=>$user])->render())->windowSize(600, 600)->setScreenshotType('jpeg', 90)->fullPage()->save('voucher/'.$code.'.png'); //Screenshot
        HomeController::makeVoucher($code);
        Mail::to( $email )->send( new ThankYouForRegistering($code) );
        return view('email.voucher',['user'=>$user]);
        //return redirect()->route('thank-you', ['code' => $code]);
    }
}
