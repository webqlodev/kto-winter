<?php

namespace App\Libraries\Payment;

class IPay88RecurringPayment
{
    /**
     * date 
     * 
     * @var string
     */
    protected $date;

    /**
     * The input filename for Bind Card
     * 
     * @var string
     */
    protected $bc_input_filename;

    /**
     * Collection of Bind Card input
     * 
     * @var array
     */
    protected $bc_input_data;

    /**
     * Constructor
     * 
     * @return void
     */
    public function __construct()
    {
        $date = new \DateTime();
        $this->date = $date->format('Ymd');
        $batch = 0; // TODO
        $this->bc_filename = sprintf('IPAY88_BC_IN_%s_%s_%02d.csv', env('IPAY88_MERCHANT_CODE'), $this->date, $batch);
        $this->bc_input_data = [];
    }

    /**
     * Bind Card: add a record
     *
     *  MerchantCode
     *  RefNo
     *  CCHolderName
     *  CCNo
     *  CCExpMonth
     *  CCExpYear
     *  IV
     *  UserEmail
     *  Remark1: Merchant’s remark field name for Remark2. Eg: Account Number
     *  Remark2: Remark1 value. Eg: 5009920
     *  IssuerBank
     * 
     * @param array $data 
     * @return void
     */
    public function bcAddRow($data)
    {
        $this->bc_input_data[] = $data;
    }

    /**
     * Bind Card: Generate input file
     * 
     * @return mixed
     */
    public function bcGenerateInput()
    {
        // must have at least 1 record
        if (empty($this->bc_input_data)) return false;

        $header = sprintf('IPAY88_BC_IN_%s', $this->date);
        $footer = sprintf('IPAY88_BC_IN_END_%s', $this->date);

        $file = $this->bc_filename; // TODO: set file path

        $fp = fopen($file, 'w');
        fputcsv($fp, $header);
        foreach ($this->bc_input_data as $row) {
            fputcsv($fp, $row);
        }
        fputcsv($fp, $footer);
        fclose($fp);
    }

    /**
     * Bind Card: Upload to iPay88 FTP server
     * 
     * @return boolean
     */
    public function bcUpload()
    {
    }

    /**
     * Subsequent Charge: add a record
     * 
     * @param array $data 
     * @return void
     */
    public function scAddRow($data)
    {
    }

    /**
     * Subsequent Charge: Generate input file
     * 
     * @return string
     */
    public function scGenerateInput()
    {
    }

    /**
     * Subsequent Charge: Upload to iPay88 FTP server
     * 
     * @return boolean
     */
    public function scUpload()
    {
    }

    /************** Helper **************/
    protected function _aesEncrypt($key, $data)
    {
        $decoded_key = base64_decode($key);
        $initialization_vector_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		if (strlen($key) > ($key_max_length = mcrypt_get_key_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC))) {
            throw new \InvalidArgumentException(sprintf('The key length must be less or equal than %d.', $key_max_length));
        }

        $block_size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $pad = $block_size - (strlen($data) % $block_size);
        $iv = mcrypt_create_iv($initialization_vector_size, MCRYPT_DEV_URANDOM);
        $ret = mcrypt_encrypt(
            MCRYPT_RIJNDAEL_128,
            $decoded_key,
            $data . str_repeat(chr($pad), $pad),
            MCRYPT_MODE_CBC,
            $iv
        );
		
        return [
            'iv' => base64_encode($iv),
            'encrypted' => base64_encode($ret),
        ];
    }
 
    protected function _aesDecrypt($key, $encrypted_data)
    {
        $decoded_key = base64_decode($key);
        $initialization_vector_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		if (strlen($key) > ($key_max_length = mcrypt_get_key_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC))) {
            throw new \InvalidArgumentException(sprintf('The key length must be less or equal than %d.', $key_max_length));
        }

        $initialization_vector = substr($encrypted_data, 0, $initialization_vector_size);
        $data =  mcrypt_decrypt(
			MCRYPT_RIJNDAEL_128,
			$decoded_key,
			base64_decode($encrypted_data),
			MCRYPT_MODE_CBC,
			base64_decode($initialization_vector)
        );
        $pad = ord($data[strlen($data) - 1]);
        return substr($data, 0, -$pad);
    }
}
