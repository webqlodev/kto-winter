<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Cache;

/**
 * App\Libraries\Geolocation
 * 
 * @package 
 * @version $Id$
 * @copyright Copyright (C) 2017 Webqlo Sdn Bhd. All rights reserved.
 * @author Js Lim <jslim@webqlo.com> 
 */
class Geolocation
{
    /**
     * Get the location info by IP address
     * 
     * @param string $ip 
     * @return array
     */
    public static function searchByIp($ip)
    {
        $json = Cache::get('geolocation.ip.' . $ip);
        if ($json) { // get from cache
            return $json;
        } else { // if no cached, then call api
            try {
                $client = new \GuzzleHttp\Client();
                $res = $client->get('http://api.ipstack.com/' . $ip .'?access_key=acd9d154a67f49860bef6a79373597dc');
                if ($res->getStatusCode() == 200) {
                    $json = json_decode((string)$res->getBody(), 1);
                    Cache::put('geolocation.ip.' . $ip, $json, '3600'); // cache for 1 day
                }
            } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            }
        }

        return empty($json) ? [] : $json;
    }
}
