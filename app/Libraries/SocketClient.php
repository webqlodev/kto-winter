<?php

namespace App\Libraries;

/**
 * App\Libraries\SocketClient
 * 
 * @package 
 * @version $Id$
 * @copyright Copyright (C) 2017 Webqlo Sdn Bhd. All rights reserved.
 * @author Js Lim <jslim@webqlo.com> 
 */
class SocketClient
{
    /**
     * Make socket call
     * 
     * @param mixed $args 
     * @return mixed
     */
    public static function run($args)
    {
        $host = env('BACKEND_SERVER_HOST');
        $port = env('BACKEND_SERVER_PORT');
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, ['sec' => 1, 'usec' => 5000]);
        $s = socket_connect($socket, $host, $port);
        $data = ['success' => false];
        if (empty($data) || !$data['success']) {
            $msg = json_encode($args);
            $len = strlen($msg);
            // send output
            socket_write($socket, $msg, $len);
            // receive output
            $output = '';
            while($bytes = socket_recv($socket, $line, 4096, MSG_WAITALL)) {
                $output .= $line;
            }
            socket_close($socket);

            // process output
            try{
                if (!empty($output)) {
                    $data = json_decode(gzdecode($output), true);
                }
            }catch(Exception $ex){
                dd([$ex, $output]);
            }
            
        }
        return $data;
    }
    public static function secondary_run($args)
    {
        $host = env('BACKEND_SERVER_SECONDARY_HOST');
        $port = env('BACKEND_SERVER_PORT');
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, ['sec' => 1, 'usec' => 5000]);
        $s = socket_connect($socket, $host, $port);
        $data = ['success' => false];
        if (empty($data) || !$data['success']) {
            $msg = json_encode($args);
            $len = strlen($msg);
            // send output
            socket_write($socket, $msg, $len);
            // receive output
            $output = '';
            while($bytes = socket_recv($socket, $line, 4096, MSG_WAITALL)) {
                $output .= $line;
            }
            socket_close($socket);

            // process output
            try{
                if (!empty($output)) {
                    $data = json_decode(gzdecode($output), true);
                }
            }catch(Exception $ex){
                dd([$ex, $output]);
            }
            
        }
        return $data;
    }


    public static function getType(String $type){
        switch($type){
            case 'instagram':
                return 'ig';
            case 'hashtag':
                return 'ht';
            case 'facebook':
                return 'fb';
        }
        return '';
    }
}
