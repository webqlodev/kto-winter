<?php

namespace App\Libraries\Analytics\Reports;

use Illuminate\Http\Request;
use App\Libraries\SocketClient;

use App\Models\SocialAccountWatchlist;
use App\Models\User;


/**
 * EmailAlertReport report
 * 
 * @uses App\Libraries\Analytics\Reports
 * @package 
 * @version $Id$
 * @copyright Copyright (C) 2017-2018 Webqlo Sdn Bhd. All rights reserved.
 * @author Js Lim <jslim@webqlo.com> 
 */
class EmailAlertReport
{
	/**
     * The user variable
     * 
     * @var App\Models\User
     */
    protected $user;

    /**
     * constructor
     * 
     * @param User $user 
     * @param array $filters 
     * @return void
     */
    public function __construct()
    {
        // $this->user = $user;
        // $this->filters = $filters;
    }

    public function getUserInfo($day) {
        return User::where('email_alert_day', '=', $day)->get();
    }

    public function getUserEmailAlert() {
        return User::where('email_alert_day', '>', 0)->get();
    }

    public function getUserAllList() {
        return User::where('id', '>', 1)->get();
    }

    public function getEmailAlert($uid, $alert_day=1, $alert_type='peak_post', $date = None) {

        // get data from backend
        $params = [
            'action'=> 'email_get',
			'alert_day'=> $alert_day,
			'alert_type'=> $alert_type,
			'uid'=> $uid,
            'duration' => $date
        ];

        $socket_results = SocketClient::run($params);
        if ($socket_results['success']) {
        	return $socket_results['data'];
        }
    }

    public function process_posts_email_alert($uid, $alert_type = []) {
        $params = [
            'action'=> 'processEmailPosts',
            'uid'=> $uid,
            'alert_type' => $alert_type
        ];

        $socket_results = SocketClient::run($params);
        if ($socket_results['success']) {
            // error required keep running.
            return $socket_results['data'];
        }
    }

}