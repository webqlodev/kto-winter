<?php

namespace App\Libraries\Analytics\Reports;

use Illuminate\Http\Request;
use App\Libraries\SocketClient;

use App\Models\SocialAccountWatchlist;
use App\Models\User;

/**
 * Overview report
 * 
 * @uses App\Libraries\Analytics\Reports
 * @package 
 * @version $Id$
 * @copyright Copyright (C) 2017-2018 Webqlo Sdn Bhd. All rights reserved.
 * @author Js Lim <jslim@webqlo.com> 
 */
class FreeOverviewReport
{
    /**
     * The user variable
     * 
     * @var App\Models\User
     */
    protected $user;

    /**
     * Social media platform, i.e
     * facebook, instagram, hashtag
     * 
     * @var string
     */
    protected $platform;

    /**
     * The social_account_watchlist.id
     * 
     * @var int
     */
    protected $profile_id;

    /**
     * constructor
     * 
     * @param User $user 
     * @param string $platform 
     * @param int $profile_id  refer to social_account_watchlist.id
     * @param array $filters 
     * @return void
     */
    public function __construct($t_page_id)
    {
        $this->platform = 'facebook';
        $this->t_action = 'fbtool';
        $this->t_page_id = $t_page_id;
        $this->offset = 8;
        $this->dateStartEnd = $this->getLastThreeMonth();
    }

    public function getLastThreeMonth(){
        $arrDate = array();
        $dDateStart = date('Y-m-01', strtotime('-3 month'));
        $dDateEnd = date('Y-m-d', strtotime('last day of previous month'));
        $arrDate['start'] = $dDateStart;
        $arrDate['end'] = $dDateEnd;
        //$arrDate = ($dDateEnd,$dDateEnd);
        return $arrDate;
    }

    /**
     * main function to generate pdf report
     * 
     * @return string output pdf file path
     */
    public function generatePdf()
    {

        // get data from backend
        $params = [
            'action'=>$this->t_action, 
            'link'=>$this->t_page_id,
            'duration'=>[$this->dateStartEnd['start'], $this->dateStartEnd['end']],
            'start'=> $this->dateStartEnd['start'], 
            'end'=>$this->dateStartEnd['end'],
            'offset'=>$this->offset
        ];

       $arr_data = SocketClient::run($params);

        if (isset($arr_data['pages']['error'])) {    
            throw new \App\Exceptions\BackendSocketException(trans('common.data_not_available'), $arr_data);
        }
        

        $view = view('analytics.socials.free-overview', [
                'platform' => $this->platform,
                'data' => $arr_data,
                'show_ad' => array_get(1, 'ad', 1), // whether to show the "Ad" button
                'interaction_content_type' => $this->getInteractionOnContentTypeData($arr_data),
                'post_content_type' => $this->getPostContentTypeData($arr_data),
                'interaction_data' => $this->getInteractionTypeData($arr_data),
                'top_posts' => $this->getTopPostsData($arr_data),
                'post_engagement_distribution' => $this->getPostEngagementDistribution($arr_data),
                'boosted_post_engagement_distribution' => $this->getBoostedPostEngagementDistribution($arr_data),
                'start' => $this->dateStartEnd['start'],
                'end' => $this->dateStartEnd['end'],
            ]);

        // generate pdf
        $html_content = $view->render();
//echo $html_content;
        $filename = sprintf('%s-%s-overview-%s-%s-%s', str_replace([' ', '(', ')', '&', ','], '-', $this->t_page_id), $this->platform, $this->t_page_id, $this->dateStartEnd['start'], $this->dateStartEnd['end']);

        $html_file = storage_path('files/' . $filename . '.html');
        $pdf_file = storage_path('files/' . $filename . '.pdf');
        file_put_contents($html_file, $html_content);

        $sh_script = '`which node` ' . base_path('bin/export-pdf.js') . ' ' . $html_file . ' ' . $pdf_file . ' A4 --showHeaderFooter=true';

        $shell_output = shell_exec($sh_script);
  
        @unlink($html_file);
 
        return $pdf_file;
    }

    /**
     * Re-construct the data for content type
     * 
     * @param mixed $arr_data  the full list data
     * @return mixed
     */
    protected function getPostContentTypeData($arr_data)
    {
        // graph data
        $g_data = [
            'id' => 'content_type',
            'title' => 'Content Type',
            'meta' => [
                'type' => 'percentage',
                'legend' => true,
            ],
            'data' => [],
            'total' => 0,
        ];

        $raw_data = array_get($arr_data, 'summary.posts');
        if (empty($raw_data)) {
            return $g_data;
        }

        $g_data['total'] = $raw_data['total']['post_count']['all'];
        $post_counts = $raw_data['total']['post_count'];
        arsort($post_counts);
        foreach ($post_counts as $type => $count) {
            if ($type == 'all') continue;
            $g_data['data'][] = [
                'name' => ucfirst($type),
                'percentage' => ($count / $g_data['total']) * 100,
                'value' => $count,
                'series' => array_search($type, ['video','link','photo','status','carousel','event','image'])
            ];
        }
        return $g_data;
    }

    /**
     * Re-construct the data for interaction % on content type
     * 
     * @param mixed $arr_data  the full list data
     * @return mixed
     */
    protected function getInteractionOnContentTypeData($arr_data)
    {       
        // graph data
        $g_data = [
            'id' => 'interaction_content_type',
            'title' => 'Interaction % On Content Type',
            'meta' => [
                'type' => 'percentage',
            ],
            'data' => [],
            'total' => 0,
        ];

        $interactions_data = array_get($arr_data, 'summary.posts');
        if (empty($interactions_data)) {
            return $g_data;
        }

        $g_data['total'] = $interactions_data['total']['post_interaction']['all'];
        $interactions = $interactions_data['total']['post_interaction'];
        arsort($interactions);
        foreach ($interactions as $type => $interaction_count) {
            if ($type == 'all') continue;
            $g_data['data'][] = [
                'name' => ucfirst($type),
                'percentage' => ($interaction_count / $g_data['total']) * 100,
                'value' => $interaction_count,
            ];
        }
//echo"<pre>g data";print_r($g_data);echo"</pre>";
        return $g_data;
    }

    /**
     * Re-construct the data for interaction by type
     * 
     * @param mixed $arr_data  the full list data
     * @return mixed
     */
    protected function getInteractionTypeData($arr_data)
    {
        // graph data
        $g_data = [
            'id' => 'interaction_type',
            'title' => 'Interaction type',
            'meta' => [
                'type' => 'percentage',
            ],
            'data' => [],
            'total' => 0,
        ];

        $raw_data = array_get($arr_data, 'summary.posts');
        if (empty($raw_data)) {
            return $g_data;
        }

        $g_data['total'] = array_sum($raw_data['total']['interaction']);
        $interactions = $raw_data['total']['interaction'];    
        foreach ($interactions as $type => $count) {
            $g_data['data'][] = [
                'name' => str_replace(['_', '-'], ' ', $type),
                'percentage' => ($count / $g_data['total']) * 100,
                'value' => $count,
            ];
        }
//echo"<pre>interaction_data";print_r($g_data);echo"</pre>";
        return $g_data;
    }

    /**
     * Re-construct the data for top posts list
     * 
     * @param mixed $arr_data  the full list data
     * @return mixed
     */
    protected function getTopPostsData($arr_data)
    {
        // graph data
        $g_data = [
            'title' => 'Most Engaged Posts',
            'meta' => [],
            'data' => [],
        ];
        $posts = array_get($arr_data, 'posts', []);
        foreach ($posts as $i => $post) {
            if ($i >= 9) break;

            $tmp = [
                'upid' => $post['upid'],
                'type' => $post['type'],
                'interactions' => $post['interactions'],
                'profile_picture' => isset($post['profile_picture'])?$post['profile_picture']:'',
                'created_timestamp' => $post['created_timestamp'],
                'is_caption_too_long' => false, // indicate whether to show "...see more"
            ];

            if ($this->platform == 'facebook') {
                $tmp['link'] = array_get($post, 'link', '');
                $tmp['picture'] = array_get($post, 'attachments.data.0.media.image.src', array_get($post, 'attachments.data.0.subattachments.data.0.media.image.src', 'full_picture'));
                $tmp['caption'] = array_get($post, 'message', '');
                $tmp['reaction_count'] = array_get($post, 'reactions.summary.total_count', 0);
                $tmp['comment_count'] = array_get($post, 'comments.summary.total_count', 0);
                $tmp['share_count'] = array_get($post, 'shares.count', 0);
                $tmp['is_boost_post'] = array_get($post, 'is_boost_post', false);
            } else if ($this->platform == 'instagram') {
                $tmp['link'] = array_get($post, 'permalink', '');
                $tmp['picture'] = array_get($post, 'permalink') . 'media/?size=l';
                $tmp['caption'] = array_get($post, 'caption', '');
                $tmp['reaction_count'] = array_get($post, 'like_count', 0);
                $tmp['comment_count'] = array_get($post, 'comments_count', 0);
                $tmp['share_count'] = 0; // don't have share
                $tmp['is_boost_post'] = array_get($post, 'is_boost_post', false);
            }

            // trim caption if too long
            $message = $tmp['caption'];
            if (empty($tmp['picture'])) {
                if (strlen($message) > 250) {
                    $tmp['is_caption_too_long'] = true;
                    $message = substr($message, 0, 250);
                }   
            } else {
                if (strlen($message) > 70) {
                    $tmp['is_caption_too_long'] = true;
                    $message = substr($message, 0, 70);
                }   
            }
            $tmp['caption'] = $message;

            $g_data['data'][] = $tmp;
        }
        return $g_data;
    }

    /**
     * Re-construct the data for post engagement distribution
     * 
     * @param mixed $arr_data  the full list data
     * @return mixed
     */
    protected function getPostEngagementDistribution($arr_data)
    {
        // graph data
        $g_data = [
            'id' => 'post_engagement_distribution',
            'title' => 'Post Engagement Distribution',
            'meta' => [
                'show_line' => true,
                'type' => 'value',
                'axis_x' => [
                    'max' => 7,
                ],
            ],
            //'data' => array_get($arr_data, 'summary.posts.growth', []),
            'data' => array(),
        ];
        return $g_data;
    }

    /**
     * Re-construct the data for boosted post engagement distribution
     * 
     * @param mixed $arr_data  the full list data
     * @return mixed
     */
    protected function getBoostedPostEngagementDistribution($arr_data)
    {
        // graph data
        $g_data = [
            'id' => 'boosted_post_engagement_distribution',
            'title' => 'Boosted Post Engagement Distribution',
            'meta' => [
                'chart_type' => 'mix',
                'show_line' => true,
                'type' => 'value',
                'threshold' => array_get($arr_data, 'boosted.base.interactions.mean', 0),
                'axis_x' => [
                    'max' => 7,
                ],
            ],
            'data' => [
                'line' => array_get($arr_data, 'summary.posts.growth', []),
                'bar' => array_get($arr_data, 'boosted.group', []),
            ]
        ];
        return $g_data;
    }
}
