<?php

namespace App\Libraries\Analytics\Reports;

use Illuminate\Http\Request;
use App\Libraries\SocketClient;

use App\Models\SocialAccountWatchlist;
use App\Models\User;

/**
 * Comparison report
 * 
 * @uses App\Libraries\Analytics\Reports
 * @package 
 * @version $Id$
 * @copyright Copyright (C) 2017-2018 Webqlo Sdn Bhd. All rights reserved.
 * @author Js Lim <jslim@webqlo.com> 
 */
class ComparisonReport
{
    /**
     * The user variable
     * 
     * @var App\Models\User
     */
    protected $user;

    /**
     * constructor
     * 
     * @param User $user 
     * @param array $filters 
     * @return void
     */
    public function __construct(User $user, array $filters)
    {
        $this->user = $user;
        $this->filters = $filters;
    }

    /**
     * main function to generate pdf report
     * 
     * @return string output pdf file path
     */
    public function generatePdf()
    {
        $ids = explode(',', $this->filters['pages']);
        if ($this->user->id == 1) { // user_id 1 is super user
            $query = SocialAccountWatchlist::whereIn('id', $ids);
        } else {
            $query = $this->user->account->socialAccountWatchlists()
                ->whereIn('id', $ids);
        }
        $pages = $query
            ->select([
               'id',
               'social_id',
               'title',
               'type',
            ])
            ->get();
        if (empty($pages)) {
            throw new \Exception(trans('social.please_select_at_least_2_profiles'));
        }

        if (count($pages) != count($ids)) {
            throw new \App\Exceptions\BackendSocketException(trans('common.data_not_available'), $pages->toArray());
        }

        $results = [];
        // get data from backend
        foreach ($pages as $pg) {
            $params = [
                'action' => 'multi',
                'keys' => ['fans', 'boosted', 'reactions', 'summary'],
                'upids' => [$pg->social_id],
                'type' => SocketClient::getType($pg->type),
            ];
            if (isset($this->filters['start']) && isset($this->filters['end'])) {
                $params['duration'] = [$this->filters['start'], $this->filters['end']];
            }

            $socket_results = SocketClient::run($params);
            if (!$socket_results['success']) continue;

            $results[$pg->id] = $socket_results['data'][$pg->social_id];
            $results[$pg->id]['custom'] = [
                'content_type' => $this->getPostContentTypeData($results[$pg->id]),
                'reactions' => $this->getReactionDistributions($results[$pg->id]),
            ];
        }
        if (empty($results)) {
            throw new \App\Exceptions\BackendSocketException(trans('common.data_not_available'), $results);
        }

        $view = view('analytics.socials.comparison', [
                'data' => $results,
                'page_ids' => $ids,
                'start' => $this->filters['start'],
                'end' => $this->filters['end'],
            ]);
        // generate pdf
        $html_content = $view->render();
        $filename = sprintf('comparison-between-%s-%s-%s', $this->filters['pages'], $this->filters['start'], $this->filters['end']);

        $html_file = storage_path('files/' . $filename . '.html');
        $pdf_file = storage_path('files/' . $filename . '.pdf');
        file_put_contents($html_file, $html_content);
        $sh_script = '`which node` ' . base_path('bin/export-pdf.js') . ' ' . $html_file . ' ' . $pdf_file;
        if (array_get($this->filters, 'show_header_footer', 0) == 1) $sh_script .= ' --showHeaderFooter=true';
        $shell_output = shell_exec($sh_script);
        @unlink($html_file);

        return $pdf_file;
    }

    /**
     * Re-construct the data for content type
     * 
     * @param mixed $data  the full list data
     * @return mixed
     */
    protected function getPostContentTypeData($data)
    {
        // graph data
        $g_data = [
            'id' => 'content_type_' . array_get($data, 'pages.upid'),
            'title' => 'Content Type',
            'meta' => [
                'type' => 'percentage',
                'legend' => true,
            ],
            'data' => [],
            'total' => 0,
        ];

        $raw_data = array_get($data, 'summary.posts');
        if (empty($raw_data)) {
            return $g_data;
        }

        $g_data['total'] = $raw_data['total']['post_count']['all'];
        $post_counts = $raw_data['total']['post_count'];
        arsort($post_counts);
        
        foreach ($post_counts as $type => $count) {
            if ($type == 'all') continue;
            $g_data['data'][] = [
                'name' => ucfirst($type),
                'percentage' => ($count / $g_data['total']) * 100,
                'value' => $count,
                'series' => array_search($type, ['video','link','photo','status','carousel','event','image'])
            ];
        }

        return $g_data;
    }

    /**
     * Return the reactions data
     * 
     * @param mixed $data 
     * @return array
     */
    protected function getReactionDistributions($data)
    {
        $reactions = [
            ['name' => 'like'],
            ['name' => 'love'],
            ['name' => 'haha'],
            ['name' => 'wow'],
            ['name' => 'sad'],
            ['name' => 'angry'],
        ];
        $total = array_get($data, 'reactions.summary.all', 0);
        foreach ($reactions as $i => $reaction) {
            $count = array_get($data, 'reactions.summary.' . strtoupper($reaction['name']), 0);
            $reactions[$i]['value'] = $count;
            $reactions[$i]['percentage'] = ($count / $total) * 100;
        }
        return $reactions;
    }
}
