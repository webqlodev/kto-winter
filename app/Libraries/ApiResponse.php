<?php

namespace App\Libraries;

/**
 * App\Libraries\ApiResponse
 *
 * Helper class for API response
 * 
 * @package 
 * @version $Id$
 * @copyright Copyright (C) 2015 Webqlo Sdn Bhd. All rights reserved.
 * @author Js Lim <jslim@webqlo.com> 
 */
class ApiResponse {

    /**
     * HTTP status code
     * 
     * @var int
     */
    protected $_status;

    /**
     * HTTP status. E.g. 404, this value will be 'Not Found'
     * 
     * @var string
     */
    protected $_subject;

    /**
     * The message that want to show to user
     * 
     * @var string
     */
    protected $_message;

    /**
     * JSON web token
     * 
     * @var string
     */
    protected $_jwt;

    /**
     * Timezone
     * 
     * @var string
     */
    protected $_timezone;

    /**
     * Custom meta data
     * 
     * @var array
     */
    protected $_custom;

    /**
     * The actual data that user requested
     * 
     * @var mixed
     */
    protected $_data;
    /**
     * The detail error 
     * 
     * @var mixed
     */
    protected $_error;

    /**
     * A look up table for HTTP status 
     */
    const HTTP_STATUS_CODE = [
        200 => 'OK',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        500 => 'Internal Server Error',
    ];

    public function __construct() {
        $this->_status = 200;
        $this->_subject = self::HTTP_STATUS_CODE[$this->_status];
        $this->_message = '';
    }

    /********* Setter **********/
    public function setStatus($status) {
        $this->_status = $status;
        if (empty($this->_subject) || in_array($this->_subject, self::HTTP_STATUS_CODE)) $this->_subject = isset(self::HTTP_STATUS_CODE[$status]) ? self::HTTP_STATUS_CODE[$status] : 'Error';
    }

    public function setSubject($subject) {
        $this->_subject = $subject;
    }

    public function setMessage($message) {
        $this->_message = $message;
    }
    public function setError($error) {
        $this->_error = $error;
    }

    public function setJWT($jwt) {
        $this->_jwt = $jwt;
    }

    public function setResult($result) {
        $this->_data = $result;
    }

    public function addCustomMeta($key, $value) {
        if (!isset($this->_custom)) $this->_custom = [];
        $this->_custom[$key] = $value;
    }

    public function removeCustomMeta($key) {
        if (!isset($this->_custom)) return;
        unset($this->_custom[$key]);
    }

    /**
     * Return the response result
     * 
     * @return Illuminate\Http\Response
     */
    public function get() {
        $meta = [
            'status' => $this->_status,
            'subject' => $this->_subject,
            'message' => $this->_message,
            'error'=> $this->_error,
            'tz' => self::getTimezone(),
        ];
        if (!empty($this->_custom)) $meta['custom'] = $this->_custom;
        if ($this->_jwt) $meta['jwt'] = $this->_jwt;
        return response([
            'meta' => $meta,
            'response' => empty($this->_data) ? (object)[] : $this->_arrayNullToEmpty($this->_data)
        ], $this->_status)
        ->withHeaders([
            'Content-Type' => 'application/json',
        ]);
    }

    /**
     * If there are 'null' value, format it to empty string
     * 
     * @param array $json_array 
     * @return array
     */
    protected function _arrayNullToEmpty($json_array) {
        $new_json_array = array();
        foreach ($json_array as $k => $v) {
            if (is_array($v)) {
                $new_json_array[$k] = $this->_arrayNullToEmpty($v);
                continue;
            }
            if ($v === null) {
                $new_json_array[$k] = '';
                continue;
            }
            $new_json_array[$k] = $v;
        }
        return $new_json_array;
    }

    private static function getTimezone()
    {
        $ip = request()->server->get('HTTP_X_FORWARDED_FOR', request()->ip());
        $json = Geolocation::searchByIp($ip);

        return isset($json['time_zone']) && !empty($json['time_zone']) ? $json['time_zone'] : '';
    }
}
