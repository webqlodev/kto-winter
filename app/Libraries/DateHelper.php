<?php

namespace App\Libraries;

/**
 * App\Libraries\DateHelper
 * 
 * @package 
 * @version $Id$
 * @copyright Copyright (C) 2017 Webqlo Sdn Bhd. All rights reserved.
 * @author Js Lim <jslim@webqlo.com> 
 */
class DateHelper
{
    /**
     * Format facebook date to desired format
     * 
     * @param string $facebook_date 
     * @param string $format 
     * @return string
     */
    public static function formatFromFacebookDate($facebook_date, $format = 'Y-m-d H:i:s')
    {
        $date = new \DateTime($facebook_date);
        return $date->format($format);
    }

    /**
     * The number of days between 2 date
     * 
     * @param string $start  start date
     * @param string $end    end date
     * @return int
     */
    public static function daysDiff($start, $end)
    {
        $from_date = new \DateTime($start);
        $to_date = new \DateTime($end);
        $interval = $from_date->diff($to_date);
        return $interval->format('%a');
    }

    /**
     * Format date to desired format
     * 
     * @param string $date 
     * @param string $format 
     * @return string
     */
    public static function format($date, $format = 'Y-m-d H:i:s')
    {
        $date = new \DateTime($date);
        return $date->format($format);
    }
}
