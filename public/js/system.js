var pathArray = window.location.href.split('/');
pathArray.splice(pathArray.length-1,pathArray.length-1);

var postUrl = "";
for (i = 0; i < pathArray.length; i++) {
	postUrl += pathArray[i];
	postUrl += "/";
}

var $uploadCrop;
// var w = $(window).width() - 40,
//     h = $(window).height() - 368;

if($(window).width() <= 800){
	console.log('less');
	// var w = $(window).width() - 40,
 //    	h = $(window).height() - 368;
 var w = $(window).width() - 40,
 h = $(window).height() - 400;
}else{
	var w = 800,
	h = 611;
}
console.log(w);
console.log(h);
/*
var basic = $('#demo-basic').croppie({
	viewport: {
		width: w,
		height: h,
		type: 'square'
	},
	boundary: {
		width: w,
		height: h
	},

	enforceBoundary: true,
	enableExif: true
});
*/
Dropzone.autoDiscover = false;
$(window).on('load',function(){
	console.log('declare');
	$(".bootstrap-select").click(function () {
		$(this).addClass("open");
	});

});
$(document).ready(function () {
	setInputFilter(document.getElementById("mobileNo"), function(value) {
  		return /^-?\d*$/.test(value); 
  	});

  	$('.mobile-menu-item').on('click',function(){
  		$('.mobile-menu').hide();
  		if($('.mobile-menu-icon').hasClass('open-menu')){
  			$('.mobile-menu-icon').removeClass('open-menu');
  			$('.mobile-menu-icon').addClass('close-menu');
  		}else{
  			$('.mobile-menu-icon').addClass('open-menu');
  			$('.mobile-menu-icon').removeClass('close-menu');
  		}
  	})

	$('.preview').on('click',function(){
		var tFrame = $(this).attr('src');
		$('#previewFrame').attr('src',tFrame);
	});

	$('#btnChooseFrame').on('click',function(){
		$('#frameModal').modal('show');
	});


	$('#btnSelectFrame').on('click',function(){
		$.LoadingOverlay("show", {
		    image       : "",
		    fontawesome : "fa fa-cog fa-spin"
		});
		var tFrame = $("#previewFrame").attr('src');
		$('#selectedFrame').attr('src',tFrame);
		$('#previewSelectedFrameImg').removeClass('d-none');
		html2canvas(document.querySelector("#previewSelectedFrameImg")).then(canvas => {
		    canvas.setAttribute("id", "canvas");
		                document.body.appendChild(canvas);
		    var myCanvas = $(document).find('#canvas');
		    var myImg = myCanvas.get(0).toDataURL();
		    $('#canvas').remove();
		    $('#previewSelectedFrameImg').addClass('d-none');
		   	$("#frame1").attr('src',myImg);
		   	$("#txtSelectedFrameImg").val(myImg);
		   	$("#frameUploadPreview").attr('src',myImg);
		   	//$('.dz-image img').addClass('upload-frame-img');
		   	$('.dz-image img').attr('src',myImg);
		   	$.LoadingOverlay("hide");
		   	$('#frameModal').modal('hide');
		});
	});

	$('#btnChooseFrame').on('click',function(){
		$('#frameModal').modal('show');
	})
	$('input').on('keyup keypress blur change',function(){
		var tId = $(this).attr('id');
		$('#'+tId+'Error').addClass('d-none');
		$('#'+tId+'Error').html('');
		$('#'+tId+'FormGroup').removeClass('error');
	});

	$('#type').on('change',function(){
		var tSelectedDropdown = $(this).val();
console.log(tSelectedDropdown);
		if(tSelectedDropdown != '#'){
			var tId = $(this).attr('id');
			$('#'+tId+'Error').addClass('d-none');
			$('#'+tId+'Error').html('');
console.log('#'+tId+'FormGroup');
			$('#'+tId+'FormGroup').removeClass('error');
		}
	})

	$(".hdrFacebookShare").on('click',function(){
		facebookShare(false);
	});

	$(document).on("click", ".sensesModal", function () {
		var sensesId = $(this).attr('data-senses');
		$("#sensesImg").attr('src','./images/senses/'+sensesId+'_Pic.png')
	});


	$('.carousel-gallery-nav').on('click',function(){
		var nPageNo = parseInt($('.gallery-nav').data('pageNo'));
		if($(this).hasClass('carousel-prev')){//-1
			nPageNo -=1;
		}else{ // +1
			nPageNo +=1;
		}
		console.log(nPageNo);
		$('.gallery-nav').data('pageNo',nPageNo);
		getGalleryImage(nPageNo);
	});

	getGalleryImage(1);
	$.datepicker.setDefaults({
		dateFormat: "dd.mm.yy",
		monthNamesShort: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
	})
	$("#dateOfBirth").datepicker({
		changeMonth: true,
		changeYear: true,
		showMonthAfterYear: false,
		yearRange: "1924:2014",
		defaultDate: $.datepicker.parseDate("dd.mm.yy", $("#dateOfBirth").val())
	});
	$('#frameModal').on('hidden.bs.modal', function () {
		//var checkedValue = document.querySelector('.frameInput:checked').attr('data-img-url');
		$("input[name='car-rd1']").each( function () {
			if($(this).is(":checked")){
				//$("#inputImg").val($(this).attr('data-img-url'));
				$('#selectedFrameImg').val($(this).attr('data-img-url'));
				$("#frameUploadPreview").attr('src',$(this).attr('data-img-url'));
			}
		});
	});
	

	$("#shareBtn").on("click",function(){
		facebookShare(true);

	});

	

		//cropper.destroy();
		// if ($("#cropImg").hasClass('cropper-hidden')){
		//     $("#cropImg").cropper("destroy");
		// }
		//$("#cropImg").cropper('reset');
	
	

	var myDropzone = new Dropzone ("#myDropzone", {
		maxFilesize: 256, // Set the maximum file size to 256 MB
		url: ".https://koreatourismorg.com/upload.php",// Rails expects the file upload to be something like model[field_name]
		autoProcessQueue: false,
		acceptedFiles: 'image/*',
		maxFiles: 1,
		addRemoveLinks: true // Don't show remove links on dropzone itself.
	});
	
	myDropzone.on("addedfile", function(file){
		reader.readAsDataURL(file);
		
	});
	
	myDropzone.on("removedfile", function(file){
		$("#chooseFrame").addClass('upload-frame-disable');
	});

	$('#entryForm').submit(function(event) {
		event.preventDefault();
		$(this).find('[type="submit"]').text('Sending').prop('disabled', true);
		var imgUrl = $('#txtSelectedFrameImg').val();
		if(imgUrl == undefined || imgUrl == ''){
			var uploadedImgUrl = $("#inputUploadedImg").val();
			if(uploadedImgUrl == undefined || uploadedImgUrl == ''){
				imgUrl  = '';
			}else{
				imgUrl  = uploadedImgUrl;
			}
		}

		if ($('input#subscribe').is(':checked')) {
			var bSubscribe = '1';
		}else{
			var bSubscribe = '0';
		}

		if ($('input#termsCondition').is(':checked')) {
			var bTermsCondition = '1';
		}else{
			var bTermsCondition = '';
		}

		if($('#type').val() == '#'){
			var tTypeVal = '';
		}else{
			var tTypeVal = $('#type').val();
		}

		$.post($(this).attr('action'), {
			fullName: $('#fullName').val(),
			dateOfBirth: $('#dateOfBirth').val(),
			mobileNo: $('#mobileNo').val(),
			email: $('#email').val(),
			type: tTypeVal,
			img  : imgUrl,
			imgDesc: $('#caption').val(),
			subscribe : bSubscribe,
			termsCondition : bTermsCondition,
			socialId : $("#fbUserId").val(),
			_token: $('input[name=_token]').val()

		}).done(function(data) {
			$('#btnSubmit').text('Submit').prop('disabled', false);
			$("#returnFormId").val(data.response.submissionId);
			
			var tFbUserId = $("#fbUserId").val(),
			tFbUserEmail = $("#fbUserEmail").val(),
			tFbUserName = $("#fbUserName").val();

			if(tFbUserId == '' && tFbUserEmail == '' && tFbUserName == ''){
				$('#loginModal').modal('show'); 
			}else{
				checkUserHasShare(tFbUserId);
			}

			getGalleryImage(1);


		}).fail(function(jqXHR, textStatus, errorThrown) {
			var objError = jqXHR.responseJSON.meta.error;

			$.each(objError,function(tKey,objError){
				$('#'+tKey+'FormGroup').addClass('error');
				$('#'+tKey+'Error').removeClass('d-none');
				$('#'+tKey+'Error').text(objError[0]);
			});
	    	//$(this).find('[type="submit"]').text('Submit').prop('disabled', false);
	    	$('#btnSubmit').text('Submit').prop('disabled', false);
	        //alert('Something went wrong when trying to communicate with server. Please try again later.');
	    });
	});


	var reader = new FileReader();
	var loopLength = 6;
	var cropper;
	reader.addEventListener('loadend',function(e){

		$("#chooseFrame").removeClass('upload-frame-disable');
		$('#inputUploadedImg').val(e.target.result);
		$('#cropImg').attr('src',e.target.result);
		$("#imgError").addClass('d-none');
		$('#imgError').html('');
console.log($('#cropImg').attr('src'));
		var  image = document.getElementById('cropImg');

		cropper = new Cropper(image, {
		  aspectRatio: 800 /611,
		  viewMode : 3,
		  cropBoxMovable  : false,
		  dragMode : 'move',
		  cropBoxResizable : false,
		  movable : true,
		  autoCropArea : 1,
		  zoomOnTouch : true,
		  responsive: true,
		  crop(event) {
console.log(cropper.getCroppedCanvas());		  	
		  },
		});

		$('#cropImgModal').modal('show');

		
	});

	$("#selectCropImg").on('click',function(){
		$.LoadingOverlay("show", {
			    image       : "",
			    fontawesome : "fa fa-cog fa-spin"
		});
		
		resp = cropper.getCroppedCanvas().toDataURL('image/jpg');
		cropper.destroy();
		
		$('#previewUploadedImage').attr('src',resp);
		$("#previewUploadedImg").attr("src",resp);
		$.LoadingOverlay("hide");
		$('#frameModal').modal('show');
		$('#cropImgModal').modal('hide');
		
	});

	$(document).on('hidden.bs.modal','#cropImgModal', function () {
	   cropper.destroy();
	   console.log('destory');
	});
});

$('#activites').owlCarousel({
	loop:true,
	margin:10,
	responsiveClass:true,
	autoWidth:false,
	nav:true,
	dots: false,
	navText : ["<img class='carousel-nav' src='../images/logo_icon/Left.png'></img>","<img class='carousel-nav' src='../images/logo_icon/Right.png'></img>"],
	responsive:{
		0:{
			items:1,
				//nav:true
			},
			600:{
				items:2,
				//nav:false
			},

			1200:{
				items:3,
				//nav:true,
				loop:false
			}
		}
	});

window.onscroll = function() {scrollFunction()};


function getGalleryImage(pageNo){
	callGalleryImageApi(pageNo);
}

function resetPagination(pageNo,totalPageNo){
	console.log(pageNo,totalPageNo);
	if(pageNo == 1){
		$('.carousel-prev').hide();
	}else{
		$('.carousel-prev').show();
	}

	if(pageNo == totalPageNo){
		$('.carousel-next').hide();
	}else{
		$('.carousel-next').show();
	}
}

function callGalleryImageApi(pageNo){
	var postUrlPath = postUrl+'getGallery/'+pageNo;
	var tAppendSpinner = "<div class='col-12'><img src='../images/Spinner.gif' class='img-fluid img-spinner'/></div>";
	$("#galleryImages").html(tAppendSpinner);
	$.get(postUrlPath, {	
		_token: $('input[name=_token]').val()

	}).done(function(data) {
		if(data.length == 0){
			$('#gallerySection').addClass('d-none');
		}else{
			resetPagination(pageNo,data.response[0].total_page);
			populateGallery(data.response);
		}

	}).fail(function(jqXHR, textStatus, errorThrown) {
		alert('Something went wrong when trying to communicate with server. Please try again later.');

	});
}

function populateGallery(objGallery){
	var galleryCaption = [],
	tAppendGallery = '',
	objGalleyCaption = {};

	$.each(objGallery,function(key, objData){

		objGalleyCaption = {};
		objGalleyCaption['ImageCaption'] = objData.img_desc;
		galleryCaption.push(objGalleyCaption);
		tAppendGallery += '<div class="col-6 col-sm-4 padbottom20 gallery-container"><a data-id="'+key+'" href="'+objData.img+'" data-lightbox="roadtrip" class="lightboxContent"><img class="img-fluid gallery-img" src="'+objData.img+'" alt="Gallery"></a></div>';
	});

	$("#galleryImages").html(tAppendGallery);
	$("#gallerySection").removeClass('d-none');
	$(".lightboxContent").on('click',function(){
		var idNo = $(this).attr('data-id');
		var imgCaption = galleryCaption[idNo]['ImageCaption'];
		lightbox.option({
			'albumLabel' : imgCaption,
			'maxHeight' : '100%'
		});
		$(".lb-number").text(imgCaption);
	});
}

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
	console.log('statusChangeCallback');
	console.log(response);

	if (response.status === 'connected') {
	  // Logged into your app and Facebook.
	  callFacebookAPI(response.authResponse.userID);
	} else {

	}
}

function checkLoginState() {
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});
}

function callFacebookAPI(tUserID) {
	FB.api(tUserID, { locale: 'tr_TR', fields: 'name, email' },function(response) {

		$("#fbUserEmail").val(response.email);
		$("#fbUserName").val(response.name);
		$('#fbUserId').val(response.id);
		var tSocialId = response.id;
		checkUserHasLogin(response.id,response.name,response.email)
	});
}

function checkUserHasLogin(tSocialId,tSocialName,tSocialEmail){
	
		var tSubmissionId = $('#returnFormId').val();
		if(tSubmissionId != ''){
			var postUrlPath = postUrl+'facebookLogin';
			$.post(postUrlPath, {
				socialId: tSocialId,
				fullName: tSocialName,
				email: tSocialEmail,
				submissionId: $('#returnFormId').val(),
				_token: $('input[name=_token]').val()
			}).done(function(data) {
				if($('#loginModal').hasClass('show')){
					$('#loginModal').modal('hide');
				}	
				checkUserHasShare(tSocialId);

			}).fail(function(jqXHR, textStatus, errorThrown) {
		       alert('Something went wrong when trying to communicate with server. Please try again later.');
		   });	
		}
		
	
}

function checkUserHasShare(socialId){
	var postUrlPath = postUrl+'checkUserHasShared';
	$.post(postUrlPath, {
		socialId: socialId,
		_token: $('input[name=_token]').val()
	}).done(function(data) {
		if(data.response.bUserIsShare == 0){
			$('#shareModal').modal('show'); 
		}else{
			$("#promoCode").text(data.response.promoCode);
			$('#codeModal').modal('show'); 
		}

	}).fail(function(jqXHR, textStatus, errorThrown) {
	       alert('Something went wrong when trying to communicate with server. Please try again later.');
	   });
}

function scrollFunction() {
	if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
		document.getElementById("backToTop").style.display = "block";
	} else {
		document.getElementById("backToTop").style.display = "none";
	}
}


// When the user clicks on the button, scroll to the top of the document
function topFunction() {
	document.body.scrollTop = 0;
	document.documentElement.scrollTop = 0;
}


function cropImage(){
	var el = document.getElementById('demo-basic');
	var resize = new Croppie(el, {
		viewport: { width: 540, height: 300 },
		showZoomer: false,
		mouseWheelZoom: 'ctrl'
	});
	resize.bind({
		url: 'https://i.imgur.com/xD9rzSt.jpg',
	});
	//on button click
	resize.result('blob').then(function(blob) {
		// do something with cropped blob
	});
}




function previewUploadImg(tElId,tAppendPreview,tInputPreview){	
	var node = document.getElementById(tElId);
	$('#'+tElId).removeClass('not-visible');
	var objDomToPng = {};
	if ((screen.width>=375)) {
		
		objDomToPng['height'] = 300;
		objDomToPng['width'] = 400;
	}else{
		objDomToPng['height'] = 280;
		objDomToPng['width'] = 320;
	}

	domtoimage.toPng(node,{height:300,width:400})
	.then(function (dataUrl) {
		console.log(dataUrl,tElId);
		//$('#'+tElId).addClass('not-visible');
		var img = new Image();
		img.src = dataUrl;
		$(tInputPreview).attr('data-img-url',dataUrl);
		$(tAppendPreview).attr('src',dataUrl);
		if(tElId.toUpperCase() == 'NOFRAME'){
			$("#inputUploadedImg").val(dataUrl);
			$('#frameUploadPreview').attr('src', dataUrl);
		}
		$("#chooseFrame").removeClass('upload-frame-disable');
		//activateModal();
	})
	.catch(function (error) {
		console.error('oops, something went wrong!', error);
	});

}

function getImgUrl(){
	var checkedValue;
	$("input[name='car-rd1']").each( function () {
		if($(this).is(":checked")){
			//$("#inputImg").val($(this).attr('data-img-url'));
			checkedValue = $(this).attr('data-img-url');
		}
		//alert($(this).val());
	});				
	return checkedValue;
}

function toggleImage(){
	$('.mobile-menu-icon').toggleClass("open-menu close-menu");
	$('.mobile-menu').slideToggle("show-menu");
}

function facebookShare(bShowPromoCode){
	FB.ui({
		method: 'share',
		display: 'popup',
		href: 'https://www.koreatourismorg.webqlo.com.my/',
	}, function(response){
		if(bShowPromoCode == true){
			if(typeof(response) != 'undefined'){
				var postUrlPath = postUrl+'facebookShare';
				$.post(postUrlPath, {
					socialId: $('#fbUserId').val(),
					_token: $('input[name=_token]').val()

				}).done(function(data) {
					$('#shareModal').modal('hide');
					$("#promoCode").text(data.response.promoCode);
					$('#codeModal').modal('show'); 

					sendEmail();
				}).fail(function(jqXHR, textStatus, errorThrown) {
			       alert('Something went wrong when trying to communicate with server. Please try again later.');
			   });
			}
		}

	});
}

function sendEmail(){
	var tSubmissionId = 	$("#returnFormId").val();
	var postUrlPath = postUrl+'sendEmail/'+tSubmissionId;
	$.get(postUrlPath, {
		_token: $('input[name=_token]').val()

	}).done(function(data) {

	}).fail(function(jqXHR, textStatus, errorThrown) {
		alert('Fail to send email');
	});
}

function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  });
}