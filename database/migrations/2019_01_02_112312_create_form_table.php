<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_form', function (Blueprint $table) {
            $table->increments('form_id');
            $table->string('t_fullname',255);
            $table->date('d_dateofbirth');
            $table->string('t_nric',255);
            $table->string('t_email',100);
            $table->string('t_phone',255);
            $table->string('t_type',255);
            $table->string('t_img',100);
            $table->string('t_img_desc',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
