<div class="modal" tabindex="-1" role="dialog"  id="cropImgModal">
    <div class="modal-dialog full-modal" role="document">
        <div class='modal-content modal-frame'>
            <div class="modal-header frame-modal-header">
                <div class="col-xs-5">
                    &nbsp;
                </div>
                <div class="col-xs-5">
                    <button type="button" class="close modal-close-button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class='col-12 upload-frame-container' id='upload'>
                    <div class='col-xs-12'>
                    <img src='#' id='cropImg' class='img-responsive uplaoded-frame-crop'/>
                    </div>
                </div>
                <div class='col-xs-12'>
                    <p class='frame-selection' id='selectCropImg'> I want to use this image</p>
                </div>
            </div>
        </div>
    </div>
</div>