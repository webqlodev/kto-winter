<div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h2 class='text-center'>
          Privacy Policy
        </h2>
        <section class="page-content">
          <p class="paragraph">VisitKorea thoroughly protects users' personal information. This Privacy Policy is subject to changes according to the laws of the Republic of Korea and the policy of VisitKorea. Users can review the most current version of the Privacy Policy at any time. </p>
          <p class="paragraph">VisitKorea is a website for global netizens interested in Korean tourism. All personal information from users is collected and used according to VisitKorea Privacy Policy. </p>
          <div class="page-section">
            <h3 class="post-title">Collecting Personal Information </h3>
            <p>VisitKorea collects personal information that new users provide when registering. VisitKorea may also collect specific information on computer hardware and software, such as users' IP addresses, type of browsers, name of domain, time of access, and website addresses. This information is used in order to manage VisitKorea, increase the service quality, and make general statistics about VisitKorea users. </p>
            <p class="paragraph">You must recognize that it may be possible for others to collect your information if you open your personal information to the public on the VisitKorea Zone or online forums. In such cases you are responsible for the exposure of your personal information. </p></div>
            <div class="page-section">
              <h3 class="post-title">Keeping and Disuse of Personal Information </h3>
              <p>While you take a service from VisitKorea as a member, VisitKorea keeps your personal information and uses it for providing better service. But if you secede from VisitKorea through the process of the membership cancellation according to the Terms of Service, then VisitKorea will remove your information from its hard disks by the method that VisitKorea cannot restore the seceder's personal information. VisitKorea will be unable to see or use it in any way. </p></div>
              <div class="page-section">
                <h3 class="post-title">Use of Personal Information </h3>
                <p>VisitKorea does not expose users' personal information to the third parties or other companies. But VisitKorea can release it if forced by legal procedures, or in case of urgent circumstances for protecting the public safety. </p>
                <p class="paragraph">VisitKorea periodically sends non-commercial news of the contents, goods and services by e-mail. Users can opt not to receive these announcements in the "Personal Member Information" section. </p></div>
                <div class="page-section">
                  <h3 class="post-title">Security Policy for the Personal Information </h3>
                  <p>User's personal information is thoroughly protected by password. VisitKorea tries to protect personal information from the use without permission by using the latest security technology. </p></div>
                  <div class="page-section">
                    <h3 class="post-title">Use of Cookies </h3>
                    <p>VisitKorea saves users' information and uses cookies on occasion for providing services adapted to the users. Cookies are text files installed in user's hard disk by website's server. </p>
                    <p class="paragraph">You can accept or reject Cookies by setting your web browser. </p></div>
                    <div class="page-section">
                      <h3 class="post-title">Children </h3>
                      <p>VisitKorea does not share the information of the children under the age of 14 with anyone or send commercial e-mail to them. </p>
                      <p class="paragraph">If a legal representative of a child under the age of 14 requests VisitKorea to see or correct children's personal information, or withdraw the consent, then VisitKorea will respond immediately. </p></div>
                      <div class="page-section">
                        <h3 class="post-title">Questions and Suggestions </h3>
                        <p>If you have opinions or complaints about the VisitKorea Privacy Policy, contact us at the following address. VisitKorea will do whatever possible to resolve any complaints or to clarify these Terms of Service. </p></div>
                        <div class="page-section">
                          <h3 class="post-title">International Smart Tourism Team </h3>
                          <p>Korea Tourism Organization <br>
                            10 Segye-ro, Wonju-si, Gangwon-do 26464<br>Republic of Korea </p></div>
                            <p class="page-information color-focus">The Privacy Policy of VisitKorea continues in effect from April 12, 2008.</p></section>

                          </div>
                          <!-- <div class="modal-footer">
                            <button type="button" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div> -->
                        </div>
                      </div>
                    </div>