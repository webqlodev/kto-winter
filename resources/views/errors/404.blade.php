@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Oops, Page Not Found!</h1>

    <p>
        Are you sure you are on the right track?
    </p>
</div>
@endsection
