<div class="modal fade" id="codeModal" tabindex="-1" role="dialog" aria-labelledby="codeModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content share-modal-background">
            
            <div class="modal-body login-modal">
                <div class='col-xs-12'>
                    <h2 class='text-center login-modal-title title'>
                        thank you
                    </h2>
                </div>
                
                <div class='col-xs-12'>
                    <p class='text-center padtop30'>
                        Get <b>5% OFF</b> On All KKday Korea related products upon entry! 
                    </p>
                    
                    <h2 class='text-center padtop20'>
                        <u>Code <span id='promoCode'></span></u>
                    </h2>
                    
                    <p class='text-center padtop5'>
                        Key in code for discount upon check out at KKday
                    </p>
                </div>
            </div>
            
        </div>
    </div>
</div>