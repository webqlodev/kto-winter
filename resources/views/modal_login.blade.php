<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content share-modal-background">
            <div class='col-xs-12 pull-right'>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body login-modal">
                <div class='col-xs-12'>
                    <h2 class='text-center login-modal-title'>
                        Thanks for your submission
                    </h2>
                    <h2 class='text-center login-modal-title'>
                        Get 5% OFF KKday promo code*!
                    </h2>
                </div>
                <div class='col-xs-12 custom-fb-button'>
                    <div class="fb-login-button" onlogin="checkLoginState();" data-scope="public_profile,email " data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
                </div>
                

                <div class='col-xs-12'>
                    <p class='text-center login-modal-note'>
                        Note : Please disable pop-up blocker in your browser before continuing to next steps.
                        <br/>
                        *Terms & Conditions Apply.
                    </p>
                </div>
            </div>
            
        </div>
    </div>
</div>