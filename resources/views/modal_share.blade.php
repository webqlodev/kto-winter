<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModal" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content share-modal-background">
            
            <div class="modal-body login-modal">
                <div class='col-xs-12'>
                    <h2 class='text-center login-modal-title'>
                        Just One More Step...
                    </h2>
                </div>
                <div class='col-xs-12'>
                    <p class='share-modal-para1'>Share to your friends to get 5% OFF KKday promo code*!</p>
                </div>
                <div class='col-xs-12 text-center btn-fb-container'>
                   <!--  <div id="shareBtn" class="fb-share-button"  data-layout="button" data-size="large" data-mobile-iframe="true"></div> -->

                    <div id="shareBtn" class="btn btn-fb-share">
                        <i class='fa fa-facebook'></i>
                        <span class='btn-fb-txt'>Share with Facebook</span>
                    </div>
                </div>
                

                <div class='col-xs-12'>
                    <p class='text-center share-modal-note'>
                        Note : Please disable pop-up blocker in your browser before continuing to next steps.
                        <br/>
                        *Terms & Conditions Apply.
                    </p>
                </div>
            </div>
            
        </div>
    </div>
</div>