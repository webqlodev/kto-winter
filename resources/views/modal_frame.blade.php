<div class="modal" tabindex="-1" role="dialog"  id="frameModal">
    <div class="modal-dialog full-modal" role="document">
        <div class='modal-content modal-frame'>
            <div class="modal-header frame-modal-header">
                <div class="col-xs-5">
                    &nbsp;
                </div>
                <div class="col-xs-5">
                    <button type="button" class="close modal-close-button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
            <div class="modal-body ">
                <table class="car-table responsive" cellpadding=0 cellspacing=0 border=0>
                    <tr>
                        <td>
                            <!--[if !mso]><!-- -->
                            <input type=radio class="cboxcheck" style="display:none !important;" checked>
                            <!--<![endif]-->
                            <div>
                                <!--[if !mso]><!-- -->
                                <div class="thumb-carousel " style="height:0px;max-height:0px;overflow:hidden;text-align:center">
                                    <label class='carousel-inline'>
                                        <input type="radio" name="car-rd1" id='inputPreview5' data-frame-index='5' class="cbox5 input-carousel frameInput"   >
                                        <span>
                                            <label class='carousel-inline'>
                                                <input type="radio" name="car-rd1" id='inputPreview4' class="cbox4 input-carousel frameInput"  data-frame-index='4'>
                                                <span>
                                                    <label class='carousel-inline'>
                                                        <input type="radio" name="car-rd1" id='inputPreview3' class="cbox3 input-carousel frameInput" data-frame-index='3' >
                                                        <span>
                                                            <label class='carousel-inline'>
                                                                <input type="radio" name="car-rd1" id='inputPreview2' class="cbox2 input-carousel frameInput" data-frame-index='2'>
                                                                <span>
                                                                    <label class='carousel-inline'>
                                                                        <input type="radio" name="car-rd1" class="cbox1 input-carousel frameInput" id='inputPreview1' data-frame-index='1'>
                                                                        <span>
                                                                            <div class="content-1 car-content show-frame">
                                                                                <!-- <img class='preview1 img-carousel' src="./images/frame/Taste_Frame.png" > -->
                                                                                <div class='col-12 upload-frame-container upload-frame-margin' id='upload'>
                                                                                    <img id='previewFrame' src='./images/frame/Sight_Frame.png' class='frame frame-preview-new' />
                                                                                    <div class='col-xs-12'>
                                                                                    <img src='#' id='previewUploadedImage' class='img-responsive uplaoded-frame-crop'/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="content-2 car-content">
                                                                                <img class='preview2 img-carousel' src="./images/frame/Taste_Frame.png"  border=0>
                                                                            </div>
                                                                            <div class="content-3 car-content">
                                                                                <img class='preview3 img-carousel' src="./images/frame/Touch_Frame.png"  border=0>
                                                                            </div>
                                                                            <div class="content-4 car-content">
                                                                                <img class='preview4 img-carousel' src="./images/frame/Sound_Frame.png"  border=0>
                                                                            </div>
                                                                            <div class="content-5 car-content">
                                                                                <img class='preview5 img-carousel' src="./images/frame/Scent_Frame.png"  border=0>
                                                                            </div>
                                                                            
                                                                            <span class="thumb thumb-1" style="display:none;">
                                                                                <img class='preview' src="./images/frame/Taste_Frame.png" width="50" style="display:block;max-height:0" border=0>
                                                                            </span>
                                                                        </span>
                                                                    </label>
                                                                          <span class="thumb thumb-2" style="display:none;">
                                                                            <img class='preview' src="./images/frame/Sight_Frame.png" width="50" style="display:block;max-height:0" border=0></span>
                                                                </span>
                                                            </label>
                                                                  <span class="thumb thumb-3" style="display:none;"><img class='preview' src="./images/frame/Touch_Frame.png" width="50" style="display:block;max-height:0" border=0></span>
                                                        </span>
                                                    </label >
                                                          <span class="thumb thumb-4" style="display:none;"><img class='preview' src="./images/frame/Sound_Frame.png" width="50" style="display:block;max-height:0" border=0></span>
                                                </span>
                                            </label>
                                            <span class="thumb thumb-5" style="display:none;"><img class='preview' src="./images/frame/Scent_Frame.png" width="50" style="display:block;max-height:0" border=0></span>
                                        </span>
                                    </label>
                                </div>
                                <div class='col-xs-12'>
                                    <p class='frame-selection hide' id='btnSelectFrame'> I want to use this image</p>
                                </div>
                                <!--<![endif]-->
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>