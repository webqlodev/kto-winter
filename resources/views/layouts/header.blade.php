@php
$whatsapp_url = 'https://wa.me/?text='.url()->current();
@endphp
<div class='header-wrapper container'>
    <div class="nav-menu nav-header row">

        <div class="col-sm-3">

            <div class="header-menu-inner-container text-left padtop20">
                <a href=".">
                    <img src="./images/logo_icon/kto_logo.png" class='img-responsive header-img'>
                </a>
            </div>

        </div>
        <div class="col-sm-9 p-0">
            <div class="custom-margin-top">
                <div class="header-menu-inner-container pull-right">
                    <ul class="m-0 custom-text-color text-right">
                        <li><a class="menu-item" href="#home">HOME</a></li>
                        <li><a class="menu-item" href="#join">JOIN</a></li>
                        <li><a class="menu-item" href="#prizes">PRIZES</a></li>
                        <li><a class="menu-item" href="#gallerySection">GALLERY</a></li>
                        <li><a class="menu-item" href="#highlight">HIGHLIGHT</a></li>
                        <li class="sharing-option">
                            <span class="custom-display menu-item"><span><i class="fa fa-share-alt" style="    font-size: 0.85em;"></i> SHARE</span></span>

                            <div class='share-img-background'>
                                <a href="#" class='hdrFacebookShare share-btn'><img class="fb-img" src="./images/logo_icon/FB_Share.png"></a> 

                                <a href="{{$whatsapp_url}}" target="_blank" class=' share-btn'><img class="fb-img" src="../images/logo_icon/Whatsapp_Share.png"></a> 
                            </div>

                            <!-- <span class="img-container">

                                <img class="sharing-list" src="http://shiseidochristmas-2018.developer.webqlo.com/img/box.png">
                                <div class=''>
                                  <a href="#" class='hdrFacebookShare'><img class="fb-img" src="./images/logo_icon/FB_Share.png"></a>  
                                </div>
                                

                                <a href="{{$whatsapp_url}}" target="_blank"><img class="whatsapp-img" src="./images/logo_icon/Whatsapp_Share.png"></a></span> -->
                            </li>
                        </ul>
                    </div>   <!-- .header-menu-inner-container --> 
                </div><!-- .header-menu-outer-container -->
            </div>

        </div> <!-- nav-menu -->

        <div class="mobile-page-header">
            <div class="mobile-header-container ">
                <img class='mobile-header-logo' src=" ./images/logo_icon/kto_logo.png ">
                <div class='mobile-menu-collapse'>
                    <img class="mobile-menu-icon open-menu" onclick="toggleImage(
                    );">
                </div>
            </div>
            <div class="mobile-menu container">
                <div class="row m-0">
                    <div class="col-12 text-center mobile-menu-border-bottom custom-mobile-padding"><a class="mobile-menu-item"  href="#home">HOME</a></div>
                    <div class="col-12 text-center mobile-menu-border-bottom custom-mobile-padding"><a class="mobile-menu-item" href="#join">JOIN</a></div>
                    <div class="col-12 text-center mobile-menu-border-bottom custom-mobile-padding"><a class="mobile-menu-item" href="#prizes">PRIZES</a></div>
                    <div class="col-12 text-center mobile-menu-border-bottom custom-mobile-padding"><a class="mobile-menu-item" href="#gallerySection">GALLERY</a></div>
                    <div class="col-12 text-center mobile-menu-border-bottom custom-mobile-padding"><a class="mobile-menu-item" href="#highlight">HIGHLIGHT</a></div>
                    <div class="col-12 text-center custom-mobile-padding"><div class="mobile-menu-item"><i class="fa fa-share-alt" style="font-size: 0.85em;"></i><span class="mobile-share-text">SHARE</span>

                        <a href="#" class='hdrFacebookShare'><img class="custom-margin-left mobile-menu-img" src="./images/logo_icon/FB_Share.png"></a>

                            <a href="{{$whatsapp_url}}" target='_blank'><img class="custom-margin-left mobile-menu-img" src="./images/logo_icon/Whatsapp_Share.png"></a>
                        </div>
                    </div>
                </div>
            </div>
    </div> <!-- mobile-page-header -->
</div>