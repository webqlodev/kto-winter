<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113623625-5"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-113623625-5');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta name="description" content="Spiked With Color. Studded With style. All Attitude. No Limits. Sign up now to amp up your look for the holidays and to receive a deluxe-size gift.*"/>

    <meta property="og:locale" content="en" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Korea Tourism Organisation Winter" />
    <meta property="og:description" content="Spiked With Color. Studded With style. All Attitude. No Limits. Sign up now to amp up your look for the holidays and to receive a deluxe-size gift.*" />
    <meta property="og:url" content="http://nars-holiday-2018.developer.webqlo.com/" />
    <meta property="og:image" content="http://nars-holiday-2018.developer.webqlo.com/images/nars_holiday_bg_mobile.png" /> 
    <meta property="og:site_name" content="Korea Tourism Organisation Winter" />

    
    <!-- Styles -->
    <link rel="icon shortcut " href="{{ asset('/images/logo_icon/favicon.ico') }}" type="image/x-icon"/>
    <link rel="stylesheet" href="{{ asset('/plugin/bootstrap-4.1.3/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{asset('/plugin/dropzone/dropzone.css')}}"/>
    <link rel="stylesheet" href="{{asset('/plugin/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/plugin/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/plugin/font-awesome-4.7.0/css/font-awesome.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/plugin/lightbox2-master/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/plugin/bootstrap-select-1.13.2/dist/css/bootstrap-select.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/plugin/jquery-ui-1.12.1/jquery-ui.css')}}"/>
    <link rel="stylesheet" href="{{asset('/plugin/Croppie-master/croppie.css')}}">
    <link rel="stylesheet" href="{{asset('/plugin/cropper-js/dist/cropper.css')}}">

    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/pretty-checkbox.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet"/>
    @stack('css')
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <!-- Modals -->
    @stack('modal')

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script  src="{{asset('/plugin/jquery-3.3.1/jquery.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>    
    <script src="{{asset('/plugin/bootstrap-4.1.3/js/bootstrap.min.js') }}"></script>
    <script src="{{asset('/plugin/jquery-ui-1.12.1/jquery-ui.js') }}"></script>
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script src="{{asset('/plugin/lightbox2-master/dist/js/lightbox.min.js')}}"></script>
    <script src="{{asset('/plugin/dropzone/dropzone.js')}}"></script>
    <script src="{{asset('/plugin/bootstrap-select-1.13.2/dist/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('/plugin/OwlCarousel2-2.3.4/dist/owl.carousel.min.js')}}"></script>
    <script src="{{asset('/plugin/jquery-loader/dist/loadingoverlay.min.js')}}"></script>
    <!-- <script type="text/javascript" src="{{asset('/plugin/Croppie-master/croppie.min.js')}}"></script> -->
  
    <script type="text/javascript" src="{{asset('/plugin/cropper-js/dist/cropper.min.js')}}"></script>
    <script src="{{asset('/js/system.js')}}"></script>

    <script>
        window.fbAsyncInit = function() {
            FB.init({
              appId            : '584433868668803',
              xfbml            : true,
              version          : 'v3.2'
          });

            FB.getLoginStatus(function(response) {
              statusChangeCallback(response);
          });

        };



        (function(d, s, id){
           var js, fjs = d.getElementsByTagName(s)[0];
           if (d.getElementById(id)) {return;}
           js = d.createElement(s); js.id = id;
           js.src = "https://connect.facebook.net/en_US/sdk.js";
           fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    @stack('js')
</body>
</html>
