<div class="footer-wrapper footer container">
    <div class="row m-0">
        <div class="col-12 col-sm-3">
            <div class="footer-menu-outer-container">
                <div class="footer-menu-inner-container-image">
                    <a href="."><img src="./images/logo_icon/kto_logo.png" class='img-responsive footer-img'></a>
                </div>
            </div>  
        </div>
        <div class="col-12 col-sm-9 footer-menu-list p-0">
            <div class="footer-menu-outer-container custom-margin-top">
                <div class="footer-menu-inner-container">
                    <ul class="m-0 text-white pull-right">
                        <li>
                            <a class="footer-item" href="http://english.visitkorea.or.kr/enu/SEV/SC_ENG_7.jsp" target='_blank'>Privacy Policy </a>
                        </li>
                        <li> | </li>
                        <li>
                            <a class="footer-item" href="#"data-toggle="modal" data-target="#termsModal">Terms and Conditions </a>
                        </li>
                        <li class='d-none d-sm-inline-block'> | </li>
                        <li class='d-none d-sm-inline-block'>Follow Us:</li>
                        <li>
                            <a class="footer-item" href="https://www.facebook.com/KTOMalaysia/" target="_blank"><img src='./images/logo_icon/FB.png' class='footer-social-media' /></a>
                        </li>
                        <li>
                            <a class="footer-item" href="https://www.instagram.com/kto_malaysia/?hl=en" target="_blank"><img src='./images/logo_icon/IG.png' class='footer-social-media'/></a></li>
                    </ul>
                </div>    
            </div>
        </div> 
        <div class="col-12 copyright">
            <p>Copyright @ Korea Tourism Organization Kuala Lumpur. All Right Reserved.</p>
        </div>
    </div>
</div>