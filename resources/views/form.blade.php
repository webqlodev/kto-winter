<div class='col-12'>
    <h2 class='title join-title'>
        join now to win a trip to korea
    </h2>
</div>
<div class='col-12 form-container container' >                
    <div class='form-border form-border-top'>
        <img src='../images/main_image/form_white_frame.png' class='img-fluid'/>
    </div>              
    <div class='form-border form-border-bottom'>
        <img src='../images/main_image/form_white_frame.png' class='img-fluid'/>
    </div>
    <form action='{{route("submit")}}/facebookLogin' id='facebookLogin'>
        <input type='hidden' id='fbUserId' />
        <input type='hidden' id='fbUserEmail' />
        <input type='hidden' id='fbUserName' />
        <input type='hidden' id='returnFormId'/>
    </form>

    <form class='padtop30' action="{{ route('submit') }}" id='entryForm' enctype="multipart/form-data">
        <h3 class='text-center form-label frame-title'>Fill in the form below</h3>
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="form-group" id='fullnameFormGroup'>
                    <label for="name" class="control-label col-12  form-label"><b>Name*(as per your NRIC)</b></label>
                    <div class="col-12">
                        <input type="text" id="fullName" name="fullName" placeholder="Jon Doe" class="form-control">
                        <span class="help-block alert alert-danger d-none" id='fullNameError'></span>
                    </div>
                </div>
            </div> <!-- Fullname -->
            
            <div class='col-md-6 col-12'>
                <div class="form-group" id='nricFormGroup'>
                    <label for="dateofbirth" class="control-label col-md-12  form-label"><b>Date of Birth*</b></label>
                    <div class="col-12">
                        <input type="text" class='form-control' id="dateOfBirth" size="30" placeholder='01-01-1990' />
                        <span class="help-block alert alert-danger d-none" id='dateOfBirthError'></span>
                    </div>
                </div>
            </div> <!-- Date of birth -->
        </div>
        
        <div class="row">
            <div class='col-md-6 col-12 '>
                <div class="form-group"  id='mobileNoFormGroup'>
                    <label for="mobileNo" class="control-label col-md-12  form-label"><b>Mobile No*</b></label>
                    <div class="col-12">
                        <input type="text" id="mobileNo" name="mobileNo" placeholder="0123457689" class="form-control">
                        <span class="help-block alert alert-danger d-none" id='mobileNoError'></span>
                    </div>
                </div>
            </div> <!-- Mobile No -->
            <div class='col-md-6 col-12 '>
                <div class="form-group" id='emailFormGroup'>
                    <label for="emailAddr" class="control-label col-md-12  form-label"><b>Email*</b></label>
                    <div class="col-12">
                        <input type="email" id="email" name="email" placeholder="abc@mail.com" class="form-control">
                        <span class="help-block alert alert-danger d-none" id='emailError'></span>
                    </div>
                </div>
            </div> <!-- Email -->
        </div>
        
        <div class="row">
            <div class="col-12 ">
                <div class="form-group" id='typeFormGroup'>
                    <label for="type" class="control-label col-md-12  form-label"><b>Pick A Cool Sense of Korea <br class='d-block d-sm-none'/>(Based on your photo)*</b></label>
                    <div class="col-12 col-md-5">

                        <select id='type' class='selectpicker form-control'>
                            <option value="#">Please Choose One</option>
                            <option value="sight">Sight</option>
                            <option value="taste">Taste</option>
                            <option value="touch">Touch</option>
                            <option value="sound">Sound</option>
                            <option value="scent">Scent</option>
                        </select>
                        <span class="help-block alert alert-danger d-none" id='typeError'></span>
                    </div>
                </div>
            </div> <!-- Dropdown -->
        </div>

        <div class='row upload-img-container' >
                <div class="col-12 col-lg-6 col- upload-img-file form-group" id='imgFormGroup'>
                    <div class="dropzone" id="myDropzone" >
                        <div class="dz-message" data-dz-message>
                            <div class='col-12' >
                                <img class='upload-img-width img-fluid' src='./images/logo_icon/drag_and_drop.png'></img>
                            </div>
                            <div class='col-12 upload-img-text'>
                                <span class="btn btn-primary btn-file theme-btn">Choose file
                                </span>
                            </div>

                        </div>
                    </div>
                    <span class="help-block alert alert-danger d-none" id='imgError'></span>
                    <!-- <p class='form-label'><span class='mobile-line-spliter'>Step 1: </span><span class='mobile-line-spliter'>Upload your photo</span></p> -->
                </div> <!-- Upload Image -->
                <!-- <div class="col-12 col-lg-3 upload-frame-disable form-group choose-frame-img" id='chooseFrame' >
                    <div class="dropzone" >
                        <div class='col-12 frame-img'>
                           <img class='upload-frame-img img-responsive' id='frameUploadPreview' src='#'></img>
                       </div>
                       <div class='col-12'>
                            <span class="btn btn-primary btn-file theme-btn" id='btnChooseFrame' >Choose Frame
                            </span>
                        </div>
                    </div>
                    <p class='form-label'><span class='mobile-line-spliter'>Step 2: </span><span class='mobile-line-spliter'>Choose a frame</span></p>
                </div>  -->

            <div class="col-12 col-lg-6 form-group img-caption">
                <label for="caption" class="control-label form-label"><b>Tell us about your photo (optional)</b></label>
                <div class="">
                    <textarea rows="9" cols="50" id="caption" name="caption" placeholder="Caption here" class="form-control"></textarea>
                </div>
            </div> <!-- Caption -->
        </div>

        <div class='row'>

            <input type='hidden' id='inputUploadedImg' />
            <input type='hidden' id='txtSelectedFrameImg' />
            <div class="col-sm-12">
                <div class='col-12 form-group' id='termsConditionFormGroup'>
                    <input type="checkbox" name="termsCondition" id="termsCondition"><span class=' form-label'> I have agreed to the </span><a href="http://english.visitkorea.or.kr/enu/SEV/SC_ENG_7.jsp" target='_blank' class='form-label' ><u>Privacy Policy</u></a><span class=' form-label'> and</span><a href="#" class='form-label' data-toggle="modal" data-target="#termsModal"> <u> Terms & Conditions</u></a>.
                    <span class="help-block alert alert-danger d-none" id='termsConditionError'></span>
                </div>
                <div class='col-12'>
                    <input type="checkbox" class=' form-label' name="subscribe" id="subscribe"> <span class=' form-label'> Subscribe to Korean Tourism Organisation newletters.</span>
                </div>
            </div> <!-- T&C -->
            {{ csrf_field() }}
            <div class='col-12 padtop20 padbottom20'>    
                <div class='col-12 btn-submit-center'>
                    <button type='submit' id='btnSubmit' class="btn btn-primary btn-file theme-btn">Submit </button>
                </div>
                <!--<input type="submit" value="Submit">-->
            </div><!-- Submit Button -->
            
            
        </div>
    </form>
    <div class='form-container-footer'></div>
</div>