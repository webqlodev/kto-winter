<div class='section highlight-section activities-background container-fluid '>
    <div class='container'>
        <div class='row'>
            <div class='col-12' id='highlight'>
                <h2 class='title padtop20'>
                    Check out some cool winter activities below also!
                </h2>
            </div>
            

            <div class='col-12 padtop20 padbottom20 col-carousel'>
                <div class='owl-carousel owl-theme carousel-container' id='activites'>

                    @include('activity-content', ['img_src' => 'https://image.kkday.com/image/get/w_1900%2Cc_fit/s1.kkday.com/product_17594/20170815035301_4Nf1Y/jpg',
                    'name' => 'Ice Fishing Day Tour from Seoul: Pyeongchang Trout Festival',
                    'no_booked' => '2.0',
                    'city'=>'Korea, Gangwon, Seoul',
                    'review_no' => '392',
                    'more_link' =>'https://www.kkday.com/en/product/17594'])

                    @include('activity-content', ['img_src' => 'https://image.kkday.com/image/get/w_1900%2Cc_fit/s1.kkday.com/product_5825/20180921095937_mNpNE/jpeg',
                    'name' => "Day Tour from Seoul: Korea's Yangji Pine Ski Resort",
                    'no_booked' => '2.0',
                    'city'=>' Korea, Gyeonggi, Seoul',
                    'review_no' => '392',
                    'more_link' =>'https://www.kkday.com/en/product/5825'])

                    @include('activity-content', ['img_src' => 'https://image.kkday.com/image/get/w_1900%2Cc_fit/s1.kkday.com/product_19828/20180827034711_LYRY1/jpg',
                    'name' => 'Everland and Jisan Forest Ski Resort Day Tour',
                    'no_booked' => '2.0',
                    'city'=>' Korea, Seoul, Gangwon',
                    'review_no' => '392',
                    'more_link' =>'https://www.kkday.com/en/product/19828'])

                    @include('activity-content', ['img_src' => 'https://image.kkday.com/image/get/w_1900%2Cc_fit/s1.kkday.com/product_19846/20180828014544_PBkvD/jpg',
                    'name' => 'Nami Island and Vivaldi Ski Resort Day Tour from Seoul',
                    'no_booked' => '2.0',
                    'city'=>' Korea, Gyeonggi, Seoul',
                    'review_no' => '392',
                    'more_link' =>'https://www.kkday.com/en/product/19846'])

                    @include('activity-content', ['img_src' => 'https://image.kkday.com/image/get/w_1900%2Cc_fit/s1.kkday.com/product_10406/20161126054217_ZEgxH/jpg',
                    'name' => 'Strawberry Picking Day Tour from Seoul: Nami Island, Petite France & Garden of Morning Calm',
                    'no_booked' => '2.0',
                    'city'=>'Korea, Gangwon, Gyeonggi, Seoul',
                    'review_no' => '392',
                    'more_link' =>'https://www.kkday.com/en/product/10406'])

                    @include('activity-content', ['img_src' => 'https://image.kkday.com/image/get/w_1900%2Cc_fit/s1.kkday.com/product_19867/20180831020639_F0xQA/jpg',
                    'name' => 'Ice Fishing Day Tour from Seoul: Hwacheon Sancheoneo Ice Festival',
                    'no_booked' => '2.0',
                    'city'=>' Korea, Gyeonggi, Seoul',
                    'review_no' => '392',
                    'more_link' =>'https://www.kkday.com/en/product/17682'])

                </div><!-- .owl-carousel -->
            </div>

        </div>
    </div>
    
</div>
