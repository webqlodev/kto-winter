@extends('layouts.email')

@section('content')
<table>
    <tr>
        <td style="font-size: 0;">
            <img alt="NARS" src="{{ asset('images/nars/email-logo.jpg') }}" style="width:100%;" />
        </td>
    </tr>
    <tr>
        <td>
            <h1>Tomorrow is the day!</h1>

            <p>
                {{ $registration->unique_code }}
            </p>
        </td>
    </tr>
</table>
@endsection
