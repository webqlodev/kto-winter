<!doctype html>
<html>
<head>
    <!--[if gte mso 7]><xml>
      <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
      </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=0.1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name=”x-apple-disable-message-reformatting”>
    <title>Thank You for Participating</title>
    <style>
        /*html{
            width: 100%;
            margin:0 auto;
        }
        body {
            -webkit-font-smoothing: antialiased;
            height: 100%;
            -webkit-text-size-adjust: none;
            margin:0 auto;
            width:100%;
        }*/
    </style>
</head>
<body bgcolor="#f6f6f6" style="padding:0; margin:0 auto; text-align:center;width:100%;height:100%">

    <div class="content" style="margin:0 auto; text-align: center;width:100%;height:100%">     
  		<div style="width: 100%;height: auto; margin: 0 auto;text-align: center;">
          {{$code}}
          {{$name}}
          {{$dateofbirth}}
          {{$mobileno}}
          {{$email}}
          {{$sense}}
          {{$photo}}
          {{$caption}}
  		</div>
    </div>

</body>
</html>