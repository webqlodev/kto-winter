@extends('layouts.email')

@section('content')
<style>
    table td{
        border-collapse:separate;
    }
</style>
<!--[if gte mso 9]>
<style type="text/css">
.full-width { width: 600px; } /* or something like that */
</style>
.bigger {
    font-size:
}
<![endif]-->
<table border='0' cellpadding="0" cellspacing="0" width="600px" class="full-width" style="width:600px;max-width:600px;margin:0 auto;border:0;border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt;so-table-rspace: 0pt;" >
    <tr>
        <td style="font-size: 0;">
            <img alt="NARS" class="full-width" src="{{ asset('images/nars/nars_climax_email_header.gif') }}" style="width:600px;" />
        </td>
    </tr>
    <tr>
        <td style="font-size: 0; position: relative;">
            <img class="full-width" src="{{ asset('images/nars/nars_climax_email_body.gif') }}" style="width: 600px;">
        </td>
    </tr>
    <tr>
        <td style="background-color: #000; text-align: center; color: #fff;padding:30px 0 35px">
            <!-- <img src="{{ asset('/images/nars/hashtag.png') }}" style="width: 48%;margin: 30px 0 5px;"> -->

            <img src="{{ asset('/images/nars/mail-p1.jpg') }}" width="345px" style="width: 345px;margin: 0 auto;">
            <p class="code" style="font-weight: bold; font-size:14pt; margin:14px auto 18px">
                NARS
            </p>
            <img src="{{ asset('/images/nars/mail-p2.jpg') }}" width="525px" style="width: 525px;margin: 0 auto;">

        </td>
    </tr>
</table>
@endsection
