

<div class='activites-box'>
    <div class='row activities-border'>   
        <img src ='{{$img_src}}' class='img-fluid'/>

        <div class='col-12 activities-title'>
            <p> {{$name}}</p>
        </div>
        <div class='col-12 activities-content'>
            <p><i class='fa fa-map-marker'></i> {{$city}}</p>
        </div>
        <div class='col-12 more-text-container'>
            <a class='more-text' href='{{$more_link}}' target='_blank'>Find Out More</a>
        </div>
        <!-- <div class='col-12 activities-content'>
            <p><i class='fa fa-fire'></i> 1.5K+  booked</p>
        </div>
        <div class='col-12 activities-content'>
            <p><i class='fa fa-map-marker'></i> Korea Seoul, Multiple cities</p>
        </div>
        <div class='col-12 col-lg-7 activities-review'>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star checked"></span>
            <span class="fa fa-star"></span>
            <span>(391)</span>
        </div>
        <div class='col-12 more-text-container'>
            <a class='more-text' href='https://www.kkday.com/en/product/17573'>Find Out More</a>
        </div> -->
    </div>
</div>