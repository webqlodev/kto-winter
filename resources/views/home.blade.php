@extends('layouts.app')

@section('content')

<div class='body-wrapper content-background'>
    @include('layouts.header')
    <div class='section home-section' id='home'>
        <div class='col-12 banner-main p-0 d-block d-sm-none' >
            <img class='img-fluid' src='./images/main_image/Main_Banner.png'></img>

        </div><!-- For mobile only -->

        <div class='col-12 banner-main p-0 d-none d-sm-block' >
            <div class='container'>
                <img class='img-fluid' src='./images/main_image/Main_Banner.png'></img>
            </div>
        </div><!-- For desktop only -->
        <div class='sub-section container'>
            <p class='sub-title'>
                Experience the winter in Korea with your 5 senses. Let's say the cool feeling of snow in your hands, the sound of winter's whistle through your ears, and the taste of hot coffee on your tongue.
            </p>
        </div>
        <div class='sub-section container p-0'>
            <h2 class='title'>
                Let's discover <br class='d-block d-sm-none' /> 5 cool senses of korea
            </h2>
            <div class='col-12 container senses-container' >
                <div class='senses-img'>
                    <a href='#' data-toggle="modal" data-target="#sensesModal">
                        <img class='img-fluid sensesModal' data-senses='Sight' src='./images/senses/Sight.png'>
                    </a>
                </div>
                <div class='senses-img'>
                    <a href='#' data-toggle="modal" data-target="#sensesModal">
                        <img class='img-fluid sensesModal' data-senses='Taste' src='./images/senses/Taste.png'>
                    </a>
                </div>
                <div class='senses-img'>
                    <a href='#' data-toggle="modal" data-target="#sensesModal">
                        <img class='img-fluid sensesModal' data-senses='Touch' src='./images/senses/Touch.png'>
                    </a>
                </div>
                <div class='senses-img'>
                    <a href='#' data-toggle="modal" data-target="#sensesModal">
                        <img class='img-fluid sensesModal' data-senses='Sound' src='./images/senses/Sound.png'>
                    </a>
                </div>
                <div class='senses-img'>
                    <a href='#' data-toggle="modal" data-target="#sensesModal">
                        <img class='img-fluid sensesModal' data-senses='Scent' src='./images/senses/Scent.png'>
                    </a>
                </div>
            </div> <!-- .senses-container -->
        </div>
    </div><!-- .home-section -->

    <div class=' container section join-section'>
        <div class='col-12' id='join'>
            <h2 class='title'>
                how to join
            </h2>
        </div>
        <div class='row'>
            <div class='col-12'>
                <img src='./images/main_image/mobile_steps.png' class='img-fluid step-image d-block d-sm-none' /> <!-- For mobile only -->

                <img src='./images/main_image/Steps.png' class='img-fluid step-image d-none d-sm-block' /><!-- For desktop only -->
            </div>  
        </div>     
    </div><!-- .join-section -->

    <div class='section container form-section'>
        @include('form') 
    </div><!-- .form-section -->

    <div class=' container section prizes-section'  id='prizes'>
        <h2 class='title prizes-title'>
            prizes
        </h2>
        <div class='row'>
            <div class='col-12 prizes-img'>
                <img src='./images/main_image/mobile_prizes.png' class='img-fluid  d-block d-sm-none' /> <!-- For mobile only -->
                <img src='./images/main_image/Prize.png' class='img-fluid d-none d-sm-block' /><!-- For desktop only -->
            </div>
        </div>
    </div>

    <div class='d-none section container gallery-section' id='gallerySection' >
        <div class='col-12' id='gallery'>
            <h2 class='title'>
                gallery
            </h2>
        </div>
        <div class='col-12 gallery-content'>
            <p class='sub-title'>
                In the meantime, let's check out the favorite cool sense from the rest
            </p>
        </div>
        <div class='gallery-image col-12'>
            <div class='gallery-nav' data-page-no=1 >
                <img class="carousel-gallery-nav carousel-prev" src="../images/logo_icon/Left.png">

                <img class="carousel-gallery-nav carousel-next" src="../images/logo_icon/Right.png">
            </div>
            
            <div id='galleryImages' class='row gallery-images text-center'>
                <div class='col-12'>
                    <img src='../images/Spinner.gif' class='img-fluid img-spinner'/>
                </div>
            </div>
        </div>
    </div>

    
    @include('activities')

    
    
</div>

@include('layouts.footer')
@include('frame')




@include('modal_code')
@include('modal_login')
@include('modal_frame')
@include('modal_crop_img')
@include('modal_share')
@include('modal_senses')
@include('modal_privacy')
@include('modal_terms_conditions')
<a href="#" id="backToTop" class="back-to-top" title="Back to top"><i class="fa fa-angle-up fa-lg"></i></a>


@endsection

@push('js')


@endpush
