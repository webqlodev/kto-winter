<div class="modal fade" id="sensesModal" tabindex="-1" role="dialog" aria-labelledby="sensesModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-body">
                <div class='col-xs-12 pull-right'>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
               <div class='col-xs-12'>
                    <img id='sensesImg' src='./images/senses/Sight_Pic.png' class='img-fluid' />
               </div>
            </div>
            
        </div>
    </div>
</div>