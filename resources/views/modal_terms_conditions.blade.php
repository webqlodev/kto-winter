<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h2 class='text-center'>
          Terms & Conditions
        </h2>
        
        <section class="page-content terms">
          <p class="paragraph">
            Korea Tourism Organization(KTO) Malaysia(MY) Cool Senses of Korea Contest 
          </p>
          <div class="page-section">
            <ol class="list-m">
              <li>
                KTO MY Cool Senses of Korea (“ The Contest”) on the KTO MY Cool Senses of Korea Website Page (“Page”) is organised by Korea Tourism Organization Kuala Lumpur (“Organiser”).
              </li>

              <li>
                This contest is open to individual citizens and permanent legal residents of Malaysia who are eighteen (18) years of age or older. Employees of the Organiser, its advertising agencies, distributors and their immediate families are not eligible to enter this Contests. The Organiser reserves the right to verify the validity of entries.
              </li>

              <li>
                The KTO MY Cool Senses of Korea Contest begins on 14th January 2019 and closes on 28th February 2019, 11:59:59PM (the “Contest Period”). Any entries after 28th February 2019, 11:59:59PM GMT+8 will be disqualified.
              </li>

              <li>
                How to enter
              </li>
              <ul>
                <li>
                  To enter the contest, entrants are required to register their personal info & choose a cool sense of Korea on KTO MY Cool Senses of Korea Website Page (<a href='https://ktowinter2019.com/'>https://ktowinter2019.com/</a>)
                </li>
                <li>
                  Entrants are required to upload photo(s) related to the sense of their choice. They may add a photo frame with Korea Winter element to enhance their entry.
                </li>
                <li>
                  Entrants must complete the Contest registration form to be qualified. Incomplete entries will be disqualified.
                </li>
              </ul>
              <li>
                By completing the Contest registration form and clicking Submit button, entrants are giving their consent to the Terms & Conditions as well as the Privacy Policy of this Contest
              </li>
              <li>
                Entrants are required to folfill and adhere to the contest mechanics and the Contest Period as stipolated on this Terms and Conditions. The Organiser reserves the right, in its sole and absolute discretion, to select winners and disqualify any entry as it sees fit in accordance with, but not limited to, the stipolated Contest mechanics and Contest Period.
              </li>
              <li>
                Entrants acknowledge that the Organiser will collect details of your social media profile to the extent necessary to conduct these Contests, and according provide your consent.
              </li>
               <li>
                Winners will be selected based on random order. Decisions of the Organiser made during the winner selection process shall be final and conclusive on all Entrants and no correspondence will be entertained.
              </li>
               <li>
                Winners will be notified via the email address provided. If winners do not reply within five (5) days of being notified, then the Organiser reserves the right to forfeit the respective prize and conduct winner reselection accordingly.
              </li>
              <li>
                Winners may claim their prizes according to the instructions provided via the winner notification email.
              </li>
              <li> 
                The “Prizes” of the Contests are as follows*:
              </li>
              
                <ul>
                    <li>
                      <b>Grand Prize: <i>1 Pair x Return Flight Tickets To Korea</i></b>
                    </li>
                    <li>
                      Consolation Prize: <i>10 units x RM50 KKday Vouchers (Korea Related Booking Only) </i>
                    </li>
                    <li>
                      *Limited to ONE (1) unit of prize per person. Moltiple entries will be considered as ONE (1).
                    </li>
                </ul>
                <li>
                 Where prizes of goods and/or services are offered, they shall be subject to availability and may not be substituted for cash. The Organiser reserves the right to offer alternative prizes of equivalent monetary value without prior notice.
                </li>
                <li>
                Prizes are not transferable and cannot be exchanged for cash or other products in part or foll.

              </li>
              <li>
                If there is a dispute as to the identity of an Entrant, the Organiser reserves the right, in its sole and absolute discretion, to determine the identity of the Entrant.
              </li>
              <li>
                The Organiser reserves the right to investigate possible cases of cheating or tampering before a winner is determined. If cheating/tampering is involved, the respective Entrant(s) will be disqualified.
              </li>
              <li>
                The information submitted by Entrants when participating in this Contest is provided to the Organiser only. This information may include your email address, mobile number and other personal information. Except as provided elsewhere in these Terms and Conditions, your personal information will only be used by the Organiser in accordance with applicable Data Protection Laws and Regolations for the purpose of administering your participation in these Contests. For more information on the Organiser’s Privacy Policy, please visit <a href='http://english.visitkorea.or.kr/enu/SEV/SC_ENG_7.jsp.'></a>
              </li>
              <li>
                Participation in the contest and/or acceptance to the prizes constitute permission to the Organiser to use the names and particolars of Entrants and Winners for marketing, advertising and trade activity purposes, and in any manner the Organiser deems appropriate.
              </li>
              <li>
                In the event of any dispute arising from this Contest or relating to the interpretation of these Terms and Conditions, the decision of the Organiser on all matters pertaining to the Contest shall be final and conclusive on all parties. No correspondence will be entertained. The Organiser reserves the right to amend the Terms and Conditions and details of any Contest and/or withdraw or terminate the Contest entirely at any time without prior notice and without any liability towards anyone.
              </li>
            </ol>
          </div>
 <!--   

          <p class="page-information color-focus">The Terms and Conditions shall enter into force on March 3, 2008.</p> -->
        </section>


      </div>



    </div>
    <!-- <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div> -->
  </div>
</div>
</div>